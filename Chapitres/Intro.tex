% Created 2023-05-29 lun. 01:40
% Intended LaTeX compiler: pdflatex
\documentclass[12pt]{article}
\usepackage[LAE, LGR, T2A, T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage[french]{babel}
\usepackage{setspace}
\usepackage{imakeidx}
\usepackage{textalpha}
\usepackage{arabtex}
\usepackage{utf8}
\usepackage[newfloat]{minted}
\usepackage[hidelinks]{hyperref}
\author{Clément Cartier\thanks{contact@catgolin.eu}}
\date{\today}
\title{Introduction}
\hypersetup{
 pdfauthor={Clément Cartier},
 pdftitle={Introduction},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.6-pre)}, 
 pdflang={French}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\maketitle
\tableofcontents
\newpage

\section{Résumé de l'état de l'art}
\label{sec:org847cd01}
Lorsqu'il a commencé à s'intéresser à l'\og astronomie alphonsine\fg{} dans les années 1980-1990,
Emmanuel Poulle a proposé d'en définir les contours à partir des canons de Jean de Saxe,
rédigés en 1327,
tels qu'ils sont reproduits dans l'édition \emph{princeps} imprimée par Erhard Ratdolt
en 1483\footnote{\og \emph{In modern times the expression
\og Alfonsine Tables\fg{} usually refers to the /edition princeps},
which includes the 1327 canons by John of Saxony./\fg{} [2 p.248].\\
\og Aujourd'hui, l'expression \fg{}tables alphonsines\og 
se réfère habituellement à l'\emph{édition princeps} qui inclut
les canons de 1327 par Jean de Saxe\fg{}}.
Les \og tables alphonsines\fg{} sont ainsi les tables dans lesquelles on trouve
res paramètres nécessaires pour effectuer les calculs prévus dans les canons
[7].
Cette définition est nécessairement arbitraire,
et une grande partie des ensembles tabulaires composés en latin
entre le \textsc{xiv}e et le \textsc{xvi}e siècle
contiennent des tables qui y répondent.
Une autre définition, toute aussi arbitraire, retenue par José Chabás
pour définir les \og tables alphonsines\fg{}, consiste à considérer
qu'il s'agit d'un large ensemble issu de la transmission et de la transformation
au travers toute l'Europe,
des tables compilées à Tolède pour le roi Alphonse X
au milieu du \textsc{xiii}e siècle\footnote{\og By Alfonsine corpus we mean the broadest category of astronomical
texts beginning in 13th-century Castile under the patronage
of Alfonso X and all texts that derive from them in some significant way\fg{}
[2 p.248]}.

Ces deux critères reposent surtout sur l'analyse des paramètres
contenus dans les tables pour délimiter le corpus \og alphonsin\fg{}.
Ils conduisent à envisager des ensembles extrêmement larges,
qui rassemble plus d'un millier de sources diverses\footnote{La base de donnée
réalisée dans le cadre du projet \textsc{Alfa} recense plus de 940 manuscrits
et 150 éditions imprimées, qui sont les manifestations de plus de 300 œuvres différentes.}.
Depuis quelques dizaines d'années,
l'histoire des tables astronomiques a donc pu étudier
un corpus très vaste, pour mieux comprendre la manière
dont les astronomes du Moyen Âge pouvaient calculer la position
et le mouvement des astres.

En l'absence d'édition critique,
les deux premières éditions imprimées
ont souvent servi de référence pour l'étude de ce corpus,
parce qu'elles présentent une version des tables
plus largement diffusée que celle des manuscrits,
dont le contenu est souvent unique.
Pour autant, les études se sont longtemps concentrées
sur la tradition manuscrite.

La question de la circulation des contenus astronomiques
entre les manuscrits et les imprimés a été ranimée
à partir du début des années 2000,
avec les débats suscités par
la publication d'un ouvrage d'Elizabeth Eisenstein
qui propose de repenser l'importance
du développement de l'imprimerie en Europe
au travers de l'émergence de ce qu'elle appelle
une \og culture des imprimés\fg{} (\emph{print culture})
[4].
L'idée d'une opposition tranchée entre
une \og culture scripturale\fg{} et une \og culture des imprimés\fg{}
ne permet pas de rendre compte de la diversité des pratiques
qui ont été employées par les acteurs et actrices
des industries du livre
entre les \textsc{xv}e et \textsc{xvii}e siècle\footnote{Voir notamment la critique de Joseph Dane [3]}.
Pour autant, l'étude de la \og culture des imprimés\fg{}
a pu servir de prétexte pour porter un nouveau regard
sur des pratiques sociales qui ont accompagné
le développement des technologies d'imprimerie
au cours de cette période.
Dans le cadre des livres de mathématiques
ou d'astronomie, on a ainsi pu mettre en avant
la manière dont, au milieu du \textsc{xvi}e siècle,
certains éditeurs de livres scientifiques
s'intègrent dans de vastes réseaux sociaux
pour favoriser la diffusion des textes
grâce à l'imprimerie\footnote{Voir par exemple
les travaux d'Ann Blair sur le cas de Conrad Gessner
à Zurich
[1], ou ceux d'Isabelle Pantin
sur les imprimeurs parisiens qui évoluent
autour d'Oronce Finé [6, 5]}.

Beaucoup des personnes qui s'engagent ainsi
dans la promotion d'une \og culture des imprimés\fg{}
s'associent également aux cercles qui font la promotion
d'une \og culture humaniste\fg{}.
La figure de Regiomontanus, proche du Cardinal Bessarion
(voir chapitre \ref{chap:ratdot}) est un exemple paradigmatique,
à laquelle les acteurs se réfèrent souvent:
c'est un mathématicien habile,
qui tente de faire comprendre à ses contemporains
comment interpréter les textes antiques,
qu'il préserve, traduit, commente,
en installant ses propres presses à Nuremberg.

Les \og humanistes\fg{} (comme elles et ils se font appeler)
cherchent ainsi à transmettre des savoirs,
des trésors intellectuels qui leur viennent du passé,
et qui leur semblent devoir être préservés.
Mais à aucun moment cette préservation
ne se résume à une simple \og mise sous cloche\fg{}:
les connaissances doivent être perfectionnées,
les textes doivent être améliorés,
adaptés aux besoins du moment.
Le but n'est pas seulement de transmettre des objets
en assurant la continuation de leur existence,
mais de faire en sorte que les acteurs et actrices
continuent de tirer de la valeur de ces objets.
C'est ce processus que je décris sous le terme de
\og patrimonialisation\fg{}.
\section{Problématique}
\label{sec:org43974f0}
Pour ce mémoire, j'ai choisi de définir les \og tables alphonsines\fg{}
comme les ensembles de tables édités en référence à la figure
du roi Alphonse, qui a régné sur les royaumes de Castille, de León et de Galice
au milieu du \textsc{xiii}e siècle.
Ce choix m'a permis de restreindre mon corpus
à seulement quelques éditions pour lui donner une dimension réaliste
dans le cadre d'un travail de mémoire.
Par ailleurs, en réfléchissant sur la place du nom d'Alphonse
dans la transmission du corpus \og alphonsin\fg{},
j'ai pu poser des questions nouvelles sur la représentation
que les acteurs et les actrices présentent de leurs tables.

Les différents processus d'édition des \og tables du roi Alphonse\fg{}
ne se résument pas à un simple placement de chiffres dans un tableau.
Les astronomes des \textsc{xv}e et \textsc{xvi}e siècles
(jusqu'au \textsc{xvii}e siècle pour l'édition de 1641)
reçoivent des tables qui ont été compilées plusieurs génération plus tôt
et se les approprient.
Lorsque certains de ces astronomes font le choix de produire une nouvelle édition,
ils tentent de transmettre les tables qu'ils ont acquises.
Les tables deviennent ainsi un patrimoine,
un objet qui se transmet à travers le temps,
et qui semble garder une certaine valeur
pour les différentes personnes qui participent à cette transmission.

La question qui traverse ce mémoire,
c'est de comprendre comment cette transmission s'est opérée
au travers des différentes éditions imprimées des \og tables alphonsines\fg{};
quel profit les acteurs et actrices ont pu espérer en entreprenant ces éditions;
comment les tables ont pu prouver leur valeur dans différents contextes scientifiques, économiques et politiques.

Ce que je prétends montrer ici,
c'est que l'édition d'un ensemble de tables
dans chacun des titres de ce corpus
n'est pas juste un assemblage de valeurs
réductibles à des fonctions mathématiques.

Toutes les tables rassemblées dans un même titre
interagissent ensemble,
et il faut comprendre comment les interactions entre ces tables
ont été pensées.
Toutes ces interactions forment un logiciel,
qui permet d'organiser des opérations,
d'effectuer des calculs, de répondre à des questions.

De plus, la description et la transmission de ces logiciels
nécessite de mobiliser des moyens matériels, techniques,
économiques et politiques.
Pour transmettre le logiciel, il faut d'abord le posséder:
avoir appris comment manipuler les tables,
et avoir accès à un moyen d'en reproduire les paramètres.
Johannes Lucilius Santritter semble avoir acquis ce savoir
à l'université de Padoue ou à Nuremberg (voir chapitre \ref{chap:santritter}),
tandis que Luca Gaurico le tient probablement
de l'université de Bologne (voir chapitre \ref{chap:gaurico}),
mais la question de l'accès au matériel de départ
est beaucoup moins clair pour les dernières éditions
(voir chapitre \ref{chap:wechel}).
Ensuite, composer les textes et paratextes
pour préparer une impression est unes entreprise coûteuse,
qui demande de la main d'œuvre qualifiée.
Réaliser les témoins demande également un approvisionnement
en matières premières.
Enfin, il faut disposer des moyens de distribuer les livres produits,
et de susciter l'intérêt du public.

En décrivant le processus de patrimonialisation
des tables alphonsines dans les cultures des imprimés
développées entre le \textsc{xv}e siècle et le \textsc{xvii}e siècle,
je propose d'étudier les tables au travers de ces deux aspects,
mathématiques et matériels,
pour mettre en évidence le lien entre les deux.
\section{Plan}
\label{sec:org6b89e49}
\end{document}
% Created 2023-05-27 sam. 18:44
% Intended LaTeX compiler: pdflatex
\documentclass[12pt]{article}
\usepackage[LAE, LGR, T2A, T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage[french]{babel}
\usepackage{setspace}
\usepackage{imakeidx}
\usepackage{textalpha}
\usepackage{arabtex}
\usepackage{utf8}
\usepackage[newfloat]{minted}
\usepackage[hidelinks]{hyperref}
\usepackage{geometry}
\onehalfspacing
\usepackage{paracol}
\geometry{a4paper, total={6.5in, 10in}}
\footnotelayout{m}
\pagestyle{plain}
\makeindex[intoc]
\author{Clément Cartier\thanks{contact@catgolin.eu}}
\date{\today}
\title{Les tables alphonsines à Paris: réédition de Luca Gaurico par Chrétien Wéchel (1545-1553)}
\hypersetup{
 pdfauthor={Clément Cartier},
 pdftitle={Les tables alphonsines à Paris: réédition de Luca Gaurico par Chrétien Wéchel (1545-1553)},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.6-pre)}, 
 pdflang={French}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\maketitle
\tableofcontents
\newpage
\section{Oronce Finé et le développement de l'imprimerie mathématique à Paris}
\label{sec:org0a5ee5e}
Pendant longtemps, on a pensé que les mathématiques n'étaient pas très bien enseignées
à Paris dans la première moitié du \textsc{xvi}e siècle\footnote{\og \emph{In recent decades, historians have begun to unravel the default
view of early sixteenth-century mathematics in Paris: that there was little to
see, less to say. We used to think this because sixteenth-century scholars said
so. Philip Melanchthon wrote in 1549 that “the disciplines of mathematics are
not so well studied in France – in fact, some of our students might earn their
living by teaching mathematics in France.”}\fg{}
(\citeprocitem{7}{Oosterhoff, 2016} p.550)\\
\og Ces dernières décénies, les historiennes et historiens
ont commencé à démêler la vision par défaut des mathématiques à Paris
au début du \textsc{xvi}e siècle, selon laquelle il n'y a pas grand chose à voir,
et moins encore à dire.
Nous avions l'habitude de penser cela parce que les universitaires du \textsc{xvi}e siècle
le disaient. Philippe Melanchton écrivait en 1549 que \fg{}les disciplines de mathématiques
ne sont pas vraiment étudiées en France. En fait, certains de nos étudiants
pourraient gagner leur vie en enseignant les mathématiques en France\og .\fg{}}.
Cependant, plusieurs travaux récents ont montré l'existence de plusieurs communautés
actives à cette époque, notamment autour des figures de Lefèvres d'Estampes
et d'Oronce Finé\footnote{Malheureusement, certaines de ces publications récentes
sont difficilement accessibles, notamment à cause de la politique de payage numérique
mise en place par certaines maisons d'éditions qui ont rendu les publications
trop coûteuses pour la plupart des bibliothèques universitaires.}.

Richard Oosterhoff montre ainsi dans un article de 2016 (\citeprocitem{7}{Oosterhoff, 2016})
que plusieurs humanistes s'engagent autour d'Oronce Finé dans une \og république des mathématiques\fg{},
à la croisée de plusieurs milieux:
artisans, universitaires, amateurs, etc.
Il décrit ainsi un espace paticulier, où la production mathématique
répond à des codes différents pour tendre à la concorde et à l'harmonie entre les différents
acteurs et différentes actrices qui participent à cette communauté scientifique.
Isabelle Pantin souligne ainsi comment, dans ce cadre,
les illustrations des manuels d'astronomie et de mathématiques se transforment
pour répondre, non seulement aux besoins de l'enseignement universitaire et scholastique,
mais aussi à celui d'autres communautés d'artisans, d'instrumenteteurs et d'instrumentatrices,
d'amateurs et d'amatrices de mathématiques (\citeprocitem{9}{Pantin, 2001}).
Après une phase au cours de laquelle la place des diagrammes avait été limitée
dans les ouvrages d'astronomie pour mieux diriger la lecture,
les nouvelles éditions des théories astronomiques par Oronce Finé
redonnent aux images une place plus importante :
elles viennent accompagner l'observation et le calcul,
en lien également avec le développement de nouveaux instruments
dédiés à l'astronomie (\citeprocitem{8}{Pantin, 2012}).

Ainsi, l'impression des ouvrages de mathématiques et d'astronomie
se développe à Paris dans la première moitié du \textsc{xvi}e siècle
autour d'une petite communauté qui circule autour des frontières de l'université,
et qui semble particulièrement intéressée par les applications des mathéamtiques
et par la fabrication d'instruments, peut-être plus que par l'apprentissage
du calcul (\citeprocitem{7}{Oosterhoff, 2016}).
C'est peut-être une des raisons pour lesquelles, en dehors de l'édition de 1545
qui est l'objet du présent chapitre,
on n'a recensé aucune édition des tables alphonsines (au sens large du terme)
à Paris.
Cependant, cette absence peut également s'expliquer par un nombre d'études
relativement moins important pour l'imprimerie scientifique parisienne de cette époque
que pour leurs collègues allemands et Italiens.
Dans les deux cas, cette absence de tables dans le contexte Parisien
me semble surprenante:
Oronce Finé est connu pour son engagement dans la promotion
des travaux de Regiomontanus, de Georg Peurbach,
et des grandes figures mathématiques du mouvement humaniste.
Pourtant, leurs tables ne semblent pas avoir été imprimées en France
avant la fin du \textsc{xvi}e siècle.
\section{Chrétien Wéchel et l'édition de 1545}
\label{sec:orgff7e80f}
\subsection{L'entreprise de Chrétien Wéchel et de Michelle Robillart}
\label{sec:org976c109}
Selon (\citeprocitem{3}{Armstrong, 1961}), Chrétien Wéchel est né en 1495
à Herentals, une petite ville du duché de Brabant,
rattaché au duché de Bourgogne depuis la première moitié du \textsc{xv}e siècle.
Son père est le prêtre Jan van Wechel. En raison de sa profession,
il n'a jamais pu se marier à Lisbeth van Eussele, mais leur union
semble être connue et acceptée par la communauté d'Herentals.
Jan et Lisbeth ont ensemble deux enfants: Chrétien et Andreas.

Chrétien Wechel travaille un temps en tant que postier
pour le libraire Conrad Resch à Paris, rue Saint-Jacques,
à côté de l'église Saint-Benoît (\citeprocitem{10}{, 1965} p.434-435).
Il est marié avec la veuve Jean Périen, Michelle Robillart
(\citeprocitem{3}{Armstrong, 1961}).
En 1526, Conrad Resch part s'installer à Bâle, et cède son commerce
à Chrétien et Michelle Robillart.
Le couple\footnote{Si le nom de Chrétien Wechel
est le seul à apparaître dans les colophons
et dans la plupart des archives,
il semble que ce soit Michelle Robillart
qui ait apporté une grande partie des fonds
pour permettre cet achat, et qu'elle s'est investie
dans le fonctionnement du commerce (\citeprocitem{3}{Armstrong, 1961}),
sans qu'il soit possible d'identifier leurs contributions
respectives.}
s'associe avec l'imprimeur Simon Du Bois,
et lorsque ce dernier quitte Paris pour s'installer à Alençon
en 1528, le couple publie un premier ouvrage à leur nom
depuis la rue Sainte-Jacques.
La même année, Chrétien Wechel est natrualisé par François Ier
à Fontainebleau.

Rapidement, les deux principales thématiques qui se dégagent
des titres publiés au nom de Chrétien Wechel sont
la Réforme Protestante, avec de nombreux textes
de Martin Luther et de ses amis,
et la grammaire grecque\footnote{On peut constater
rapidement cette tendance en recherchant le nom de Wechel
dans la base de donnée de l'\emph{Universal Short Title Catalogue}.}.

En 1530, le Collège Royal est innauguré à deux rues
du commerce de Chrétien Wechel, dans le collège de Cambrai
(\citeprocitem{6}{Guyard \& Lichon, 2003}).
Oronce Finé y enseigne les mathématiques,
et plusieurs enseignements sont également prévu
pour le grec et les langues anciennes.
Dans les années suivantes, Chrétien Wechel devient
le principal imprimeur des textes grecs qui y sont lus,
(\citeprocitem{5}{Constantinidou, 2020}).

En 1539, le couple Wechel ouvre une seconde presse
au sein du jeu de paume de saint-Jean-de-Latran,
rue Saint-Jean-de-Beauvais,
dans la maison de Jean Perier,
le premier mari de Michelle Robillart.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../images/illustrations/rue_st_jacques.png}
\caption{Section de la rue Saint Jacques près de l'église Saint-Benoist (en rouge), jeu de paume de Saint-Jean-de-Latran et rue Saint-Jean-de-Beauvais (en jaune) et emplacement du Collège Royal (en violet) sur le plan de Paris de Truschet et Hoyau de 1550 (\url{https://commons.wikimedia.org/wiki/File:UBBasel\_Map\_155u\_Kartenslg\_AA\_124.tif})}
\end{figure}

L'entreprise semble bien fonctionner:
dans l'\emph{Universal Short Title Catalogue},
on compte 53 éditions au nom de Chrétien Wechel
entre 1522 et 1529, puis 194 entre 1530 et 1539
après l'ouverture du Collège Royal.
La majorité de sa production est concentrée
sur des livres de grammaire et de littérature,
principalement en grec et en hébreux.
On trouve également un très grand nombre d'ouvrages
de médecine, probablement destinés à l'université,
dont beaucoup sont également en grec,
et des textes religieux, surtout protestants.
Une portion moins importante de sa production
traite également de mathématiques et d'astronomie:
il propose cinq éditions de traités de géométrie
ou d'arithmétique avant 1545, principalement en grec,
et sept titres d'astronomie en grec et en latin
avant 1540.

Chrétien Wechel et Michèle Robillard semblent également
disposer d'un très large réseau pour se procurer
et pour distribuer des livres à travers l'Europe,
principalement composé de protestants, probablement germaniques
(\citeprocitem{4}{Bach, n.d.}).

Mais en 1543, en pleine préparation du Concile de Trente,
la Faculté de Théologie publie une liste
d'ouvrages dont l'impression est interdite
pour lutter contre la promotion des thèses protestantes.
Une grande partie du catalogue de l'imprimeur
figure dans cette liste.
À partir de cette date,
le nombre d'ouvrages au nom de Chrétien Wechel
se réduit très drastiquement.
C'est dans ce contexte qu'il imprime en 1543
une édition des \emph{Theoricae novae planetarium}
de Georg Peurbach\footnote{\url{https://www.ustc.ac.uk/editions/153764}},
puis les tables alponsines en 1545.

\subsection{L'édition de 1545}
\label{sec:org143c279}
L'\emph{Universal Short Title Catalogue} recense 36 bibliothèques
qui préservent un exemplaire de l'ouvrage\footnote{\url{https://www.ustc.ac.uk/editions/149299}}, dont trois exemplaires numérisés.

L'ouvrage comporte un premier cahier de quatre folio (a),
puis trente trois cahiers de quatres folio (A-KK)
et un cahier final de six folio (LL).

Sur la première page, on peut lire le titre
\begin{quote}
\begin{paracol}{2}
Divi Alfonsi Romanorum et Hispaniarum regis,
astronomicæ tabulæ in propriam integritatem restituæ,
ad calcem adiectis tabulis
quæ in postrema editione deerant, cum plurimorum locorum
correctione, et accessione variarum tabellarum ex diversis autoribus
huic operi insertarum, cum in usus ubertatem, tum
difficultatis subsidium: quorum nomina summa pagellis
quinta, sexta et septima describuntur. Qua in re Paschasius
Hammelius Mathematicus insignis idemque; Regius professor
sedulam operam suam præstitit.
(\citeprocitem{1}{4 Eph. astr. 3, 1545} f.a1r)
\switchcolumn
Tables astronomiques du divin Alphonse, roi des Romains
et des Espagnes,
restituées dans leur juste intégralité,
avec l'adjonction des tables qui manquaient à la dernière édition,
avec des corections en plusieurs lieux
et l'adjonction de tables variantes de divers auteurs
insérés dans cet ouvrage,
à la fois pour enrichir l'usage,
et pour en soulager la difficulté,
dont les noms sont écrits en haut des sixième et septième pages,
dans lesquelles le mathématicien Pasquier Duhamel
est également distingué:
le professeur royal a fait diligemment son travail.
\end{paracol}
\end{quote}
Pasqualier Duhamel (\emph{Paschasius Hammelius})
mentionné ici est professeur au Collège royal à partir de 1540,
où il travaille avec Oronce Finé.

La marque et le colophon de l'imprimeur sont ajoutées
en-dessous du titre.

La première partie de l'ouvrage
reproduit le travail de Luca Gaurico (voir chapitre \ref{chap:gaurico}),
à commencer par la préface sur les deux faces du folio a2.
Les deux folios après la couture du cahier a donnent une table des matières.
Les tables commencent au folio A1r,
et s'enchaînent dans le même ordre que dans l'édition de 1524
jusqu'à la première page du cahier HH.

Les tables de calendrier, calculées pour certaines
jusqu'en 1540 (voir chapitre \ref{chap:gaurico})
ne sont pas mises à jour.

À partir du folio HH2r, au lieu des tables de la Reine Catholique,
on trouve les canons de Johannes Lucilius Santritter
(\emph{Tempus quodlibet, et eram quamlibet ex tabulis ad hoc factis extrahere\ldots{}}),
précédés par le titre
\begin{quote}
\begin{paracol}{2}
Tabulas subsequentes in proxima editione desideratas cum suis canonibus operæprecium visum est ad operis integritatem conservandam, in cale restituere, quo tantum opus omnibus numeris absolutum præstaremus.
(\citeprocitem{1}{4 Eph. astr. 3, 1545} f.HH2r)
\switchcolumn
Nous avons souhaité ajouter les tables suivantes
au sein de cette édition,
avec leurs canons,
qui sont vus comme un travail précieux,
afin de maintenir l'intégrité de l'œuvre et de la restituer de manière à ce que nous puissions la diffuser en grand nombre.
\end{paracol}
\end{quote}
Du folio HH2r jusqu'au folio II1r,
on trouve les canons pour la conversion entre les ères.
Ils sont suivis par les tables de la différence entre le début
de chaque ère et par les tables de conversion entre les ères,
jusqu'à la fin du folio LL1v.

Le folio LL2 contient les canons qui étaient ajoutés
dans le cahier e de l'édition de 1492 (voir chapitre \ref{chap:santritter}),
puis la table des climats (folio LL3),
la table de la quantité de jour\footnote{voir Chapitre \ref{chap:stantritter}} (folio LL4)
et les deux tables de l'équation du jour
qui se trouvaient déjà dans l'édition de 1492,
sur le folio LL5.

Le recto du folio LL6 est vierge,
et la marque de l'imprimeur est apposée au verso.

\subsection{Édition de 1553}
\label{sec:orgc8876fe}
Les affaires de la librairie
ne semblent pas être sauvée par l'édition de 1545,
puisque l'adresse de la rue Saint-Jacques est perdue
en 1546 (\citeprocitem{10}{, 1965} p.435),
et le nombre d'ouvrages produits avec le nom
de Chrétien Wechel diminue drastiquement
au cours des années 1540-1553.

Une seconde édition des tables alphonsines
est imprimée en 1553\footnote{\url{https://www.ustc.ac.uk/editions/158711}},
dont un exemplaire est préservé
à la \emph{Biblioteca Universidad de Sevilla}
(\citeprocitem{2}{Res.18/4/17, 1553}).
La seule différence avec la version de 1545 est la date
sur la page de garde,
et l'adresse de l'imprimeur.

Chrétien Wechel meurt l'année suivante,
avant le 18 avril 1554 (\citeprocitem{10}{, 1965} p.434).
Son fils, André Wechel, prend sa suite la même année.
\section{Conclusion}
\label{sec:org452b07f}
Contrairement aux précédentes éditions des tables alphonsines
en Italie, les deux éditions proposées par Chrétien Wechel
apparaissent très isolées en France, et il me semble
qu'une étude plus approfondie du contexte Parisien
serait nécessaire pour comprendre les raisons de cette édition.

Le choix de baser l'édition sur celle de Luca Gaurico
parrue vingt ans plus tôt peut sembler assez étrange.
Les tables de calendrier incluses dans l'édition,
qui permettaient de montrer l'utilité de la science astronomique,
auraient dûes être mises à jour pour servir ce même but.
Tout porte à croire que cette édition vient surtout
combler un manque de matériel tabulaire pour l'astronomie
à Paris à cette époque.

Les canons de Luca Gaurico ont l'avantage d'être très pédagogues.
L'édition avait été conçue pour guider le lecteur
ou la lectrice, et pour limiter l'effort de lecture.
C'est probablement la raison pour laquelle cette version
des tables alphonsines a pu sembler être la plus adaptée
au contexte de la \og république des mathématiques\fg{}.
Par ailleurs, en remplaçant les tables de la Reine Catholique
par les tables du cahier e de Johannes Lucilius Santritter
et en ajoutant les tables pour la conversion entre calendriers,
l'éditeur démontre son attachement à transmettre
un ouvrage qui conserve une certaine fidélité avec leur histoire.

Dans le cadre des enseignements du Collège royal,
et d'une activité éditoriale de Chrétien Wechel très tournée
vers l'histoire et les langues de l'Antiquité,
on peut également supposer que la reproduction du livre de Luca Gaurico
et des canons de Johannes Lucilius Santritter
ne visent plus à favoriser le calcul de tables d'astrologie,
mais plutôt à retrouver les connaissances passées sur l'astronomie.
\section{Bibliographie}
\label{sec:orgaaa434b}
\subsection{Sources primaires}
\label{sec:orgbed96da}
\hypertarget{citeproc_bib_item_1}{[1] 4 Eph. astr. 3, \textit{Divi Alphonsi Romanorum at Hispaniarum regis}. (1545) Chrétien Wechel, Paris. (en ligne: \url{https://www.digitale-sammlungen.de/en/view/bsb10158943}).\newline Munich, Bayerische Staatsbibliothek.}

\hypertarget{citeproc_bib_item_2}{[2] A Res.18/4/17, \textit{Divi Alphonsi Romanorum at Hispaniarum regis}. (1553) Chrétien Wechel, Paris. (en ligne: \url{https://fama.us.es/permalink/34CBUA_US/3enc2g/alma991005191089704987}).\newline Biblioteca universitaria de Sevilla.}
\subsection{Littérature secondaire}
\label{sec:org1f6c7a7}
\hypertarget{citeproc_bib_item_3}{[3] Elizabeth Armstrong, « the Origins of Chrétien Wechel Re-examined ». in. \textit{Bibliothèque d’humanisme et renaissance},. vol. 23,. p. 341‑346. 1961. (\url{https://www.jstor.org/stable/20674296}).}

\hypertarget{citeproc_bib_item_4}{[4] Maximilian Bach, « André Wechel (15. - 1581): Enquête bio-bibliographique ». \url{https://eman-archives.org/Mythologia/andre-wechel}.\newline Consulté le 27 mai 2023.}

\hypertarget{citeproc_bib_item_5}{[5] Natasha Constantinidou, « Libri Graeci: Les livres grecs à Paris au XVIe siècle ». \url{https://histoirelivre.hypotheses.org/author/natashaconstantinidou}. 2020.}

\hypertarget{citeproc_bib_item_6}{[6] Laurent Guyard \& Anne-Aimée Lichon, « Le collège de Cambrai et les maisons particulières du Moyen-Âge à l’èpoque moderne » in Laurent Guyard (éd.) - \textit{Le collège de france (paris)}. (2003) Éditions de la Maison des sciences de l’homme, Paris (France). ISBN 9782735126439 (en ligne: \url{http://dx.doi.org/10.4000/books.editionsmsh.21802}).}

\hypertarget{citeproc_bib_item_7}{[7] Richard Oosterhoff, « Lovers in Paratexts: Oronce Fine’s Republic of Mathematics ». in. \textit{Nuncius},. vol. 31,. p. 549‑583. 2016.}

\hypertarget{citeproc_bib_item_8}{[8] Isabelle Pantin, « The First Phases of the Theoricae Planetarum Printed Tradition (1474-1535). The Evolution of a Genre Observed through Its Images ». in. \textit{Journal for the history of astronomy},. vol. 43,. p. 3‑26. 2012.}

\hypertarget{citeproc_bib_item_9}{[9] Isabelle Pantin, « L’illustration des livres d’astronomie à la Renaissance: L’évolution d’une discipline à travers ses images » in \textit{Immagini per conoscere: Dal Rinascimento alla rivoluzione scientifica. instituto Nazionale di Studi sul Rinascimento, atti di convegni, 21}. (2001) Leo S. Olschki Editore, Florence (Italie).}

\hypertarget{citeproc_bib_item_10}{[10] \textit{Répertoire des imprimeurs parisiens, libraires, fondeurs de caractères et correcteurs d’imprimerie: Depuis l’introduction de l’Imprimerie à Paris (1470) jusqu’à la fin du seizième siècle}. (1965) M.J. Minard, Lettres modernes, Paris (France). (en ligne: \url{https://archive.org/details/bnf-bpt6k65711282/page/435/mode/1up}).}
\subsection{Bad biblio}
\label{sec:org290027b}
\end{document}
% Created 2023-05-25 jeu. 17:50
% Intended LaTeX compiler: pdflatex
\documentclass[12pt]{article}
\usepackage[LAE, LGR, T2A, T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage[french]{babel}
\usepackage{setspace}
\usepackage{imakeidx}
\usepackage{textalpha}
\usepackage{arabtex}
\usepackage{utf8}
\usepackage[newfloat]{minted}
\usepackage[hidelinks]{hyperref}
\usepackage{geometry}
\onehalfspacing
\usepackage{paracol}
\geometry{a4paper, total={6.5in, 10in}}
\footnotelayout{m}
\makeindex[intoc]
\author{Clément Cartier\thanks{contact@catgolin.eu}}
\date{\today}
\title{Peter Liechtenstein: un renouveau de la tradition alphonsine (1494--1521)?}
\hypersetup{
 pdfauthor={Clément Cartier},
 pdftitle={Peter Liechtenstein: un renouveau de la tradition alphonsine (1494--1521)?},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.6-pre)}, 
 pdflang={French}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\maketitle
\tableofcontents
\newpage
\section{Introduction}
\label{sec:orgc290e6e}
\index{Luc'antonio Guinti}
La fin de la période incunable à Venise est marquée par de nouvelles transformations
de l'industrie des imprimés à la Sérénissime.
Par le jeu des alliances et des héritages,
les compagnies allemandes qui avaient imposé un monopole
sur toute la seconde moitié du \textsc{xv}e siècle
(voir chapire \ref{chap:ratdolt}) laissent peu à peu la place
à des compagnies italiennes.
Faute de descendance légitime directe,
les premières générations des familles d'imprimeurs et imprimeuses germaniques
immigrées à Venise peinent à transmettre leur patrimoine, et des familles italiennes
prennent le relais\footnote{Voir (\citeprocitem{9}{Kikuchi, 2016} p.136-137)}.
C'est le cas par exemple de Luc'antonio Guinti (1457-1538), originaire de Florence,
qui immigre à Venise avec son frère en 1477 (voir chapitre \ref{chap:gaurico}).
Ensemble, ils construisent une vaste entreprise d'édition,
installée dans plusieurs villes italiennes (\citeprocitem{5}{Nuovo, 2013} p.51).
Avec la fin de la domination allemande sur le marché Vénitien,
la nouvelle génération d'imprimeurs et d'imprimeuses issue de l'immigration allemande
tente de masquer ses origines étrangères:
elle adopte des noms italiens, change les colophons, et abandonne
les caractères germaniques pour développer un style plus en phase avec la mode italienne.

\index{Peter Liechtenstein}
La famille Liechtenstein est une exception à ce tableau.
En 1494, Hermann Liechtenstein, imprimeur allemand installé à Venise,
dresse un testament pour léguer sa compagnie à son neveu,
Peter Liechtenstein (\citeprocitem{9}{Kikuchi, 2016} p.134).
Ce dernier semble avoir été formé dans les ateliers de son oncle.
Dans la base de données de l'\emph{Universal Short Title Catalogue}
gérée par l'\emph{University of St Andrews},
on peut voir que la date de 1494 correspond également à la date
des dernières publications d'Hermann Liechtenstein,
et à la date des premiers ouvrages édités par Peter Liechtenstein.
Ce dernier publie beaucoup d'ouvrages d'astrologie:
\emph{Kalendarium}, almanachs, etc.
Dans les titres de ces ouvrages\footnote{Tels qu'ils sont reportés
dans la base de données de l'\emph{Universal Short Title Catalogue}},
le nom de Regiomontanus leur est souvent accolé.
Par ailleurs, on trouve également à son actif
un certain nombre d'ouvrages dédiés à l'enseignement universitaire,
comme les Opuscules de Thomas D'Aquin en 1508\footnote{\url{https://www.ustc.ac.uk/editions/859388}},
ou l'Almageste de Ptolémée en 1511\footnote{\url{https://www.ustc.ac.uk/editions/851483}}.
Enfin, une grosse partie de sa production concerne des des guides de prières
(\emph{Directorium}).
C'est l'un des rares imprimeurs à continuer à utiliser des caractères gothiques,
et à revendiquer son origine germanique dans le colophon.
Enfin, l'activité de l'imprimeur est principalement centrée
autour de collaborations nouées avec d'autres presses
gérées par des personnes issues de l'immigration allemande,
et très peu avec les nouvelles presses italiennes qui s'installent à Venise
(\citeprocitem{9}{Kikuchi, 2016}).
On peut donc supposer que la clientèle ciblée par la librairie Liechtenstein
consiste surtout en étudiants, clercs et astrologues issus
de l'immigration germanique,
ou bien des étudiants d'universités germaniques.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../images/illustrations/liechtenstein.jpeg}
\caption{\label{fig:org53fa989}Marque de Peter Liechtenstein sur le folio E4v de l'exemplaire des tables de la reine Élisabeth (\citeprocitem{2}{4435, 1503} f.E4v)}
\end{figure}

\section{Les tables de la reine catholique}
\label{sec:org5d5475e}
\index{Isabelle la Catholique}
\index{Alexandre VI}
Pendant ce temps, en Espagne, Isabelle de Trastamare (1451-1504) se considère reine de Castille et de León à partir de 1474.
Mariée en 1469, contre l'avis du Pape Paul II\footnote{Le Pape Paul II est élu au cours du conclave de 1464,
notamment contre le cardinal Bessarion. Il est connu pour son hostilité envers les humanistes,
parmi lesquels on compte le seul cardinal Espagnol, Rodrigo de Borjia
(1431-1503, devenu Alexandre VI au conclave de 1492), qui défend le mariage d'Isabelle de Castille.},
à son cousin issu de germain, Ferdinand (1452-1516)\footnote{Voir figure \ref{fig:org6f2193d}},
elle devient reine consort d'Aragon en 1479 (puis de Naples en 1503).
Ainsi, le couple règne sur presque tout le territoire de la péninsule ibérique
sous une union personnelle, à l'exception des régions contrôlées par le Portugal,
par le royaume de Navarre, et par l'émirat de Grenade.
Après s'être sorti des guerres de succession,
le couple cherche à asseoir sécuriser son influence en Espagne et en Europe,
en s'affichant en défenseurs de la foi catholique (\citeprocitem{7}{Pérez, 2004}).
La prise de Grenade en 1492 leur permet d'être reconnus
par les dirigeants des autres monarchies européennes, et de nouer de nouvelles alliances.
La même année, le cardinal Rodrigo de Borjia est élu pape
contre le cardinal Giuliano Della Rovere, probablement avec le soutien du cardinal
Ascanio Sforza.

\index{Isabelle la Catholique}
\index{Famille Sforza}
\index{Alexandre VI}
\index{Guerres d'Italie}
\index{Habsbourg!Frédéric V}
\index{Habsbourg!Maximilien}
En 1493, Isabelle d'Aragon (1470-1524), fille du cousin de Ferdinand d'Aragon,
le roi de Naples Alphonse II, est mariée au duc de Milan, Jean Galéas Sforza (1469-1494).
Isabelle d'Aragon entre alors en conflit avec les dirigeants du duché de Bari.
Ces derniers ont juré allégeance au roi de France, qui revendiquent la souveraineté
sur le royaume de Naples, et qui attaquent donc le duché de Milan.
Les Sforza obtiennent le soutien du Pape, ce qui pousse le cardinal Giuliano Della Rovere
à s'allier à la France pour marcher sur Rome, sans pour autant parvenir
à renverser le Pape.
C'est le début des guerres d'Italie, au cours desquelles les deux monarques espagnols
utilisent leur influence pour renforcer leurs positions dynastiques
à Naples et à Milan, et tenter de développer leur influence vis-à-vis de l'Église
(\citeprocitem{8}{Pérez, 1988}).
Dans ce contexte, ils organisent en 1496 le double-mariage de leur fils et principal héritier,
Jean (1478-1497) et de leur fille, Jeanne (1479-1555),
avec les deux enfants du fils de l'empereur Frédéric III du Saint-Empire\footnote{C'est-à-dire
Frédéric V de Habsbourg},
Maximilien de Habsbourg (1508-1519),
alors duc de Bourgogne\footnote{La question de son allégeance au roi de France
en tant que duc de Bourgogne
est l'un des enjeux du conflit en Italie}, archiduc d'Autriche et roi des Romains.
La même année, le Pape Alexandre VI leur accorde,
par la bulle \emph{Si convenit} du 19 décembre 1496, le titre de \og rois catholiques\fg{},
en référence au titre de \og rois très chrétiens\fg{} accordé aux rois de France,
pour leur défense de la chrétienté après la prise de Grenade (\citeprocitem{8}{Pérez, 1988}).

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../images/illustrations/genealogie.png}
\caption{\label{fig:org6f2193d}Généalogie des \og rois catholiques\fg{} \tiny(image réalisée avec Dia)}
\end{figure}

\index{Alfonsó de Córdoba}
\index{Alexandre VI}
Dans ce contexte, la famille Borja bénéficie des services d'un astronome,
médecin et astrologue d'origine espagnole\footnote{Il se fait appeler \emph{Hispalensis},
c'est-à-dire \og le sévillian\fg{}, notamment par Copernic. Voir (\citeprocitem{4}{Chabás, 2004})},
Alfonsó de Córdoba (b.1458\footnote{Dans le \emph{Lumen caeli} cité plus bas,
l'auteur prend la date du 22 juillet 1458 pour donner un exemple
du calcul de la position de Saturne. José Chabás pointe dans un article de 2004
qu'une pratique courante des astronomes de cette époque consiste à prendre
leur date de naissance pour ce calcul (\citeprocitem{4}{Chabás, 2004} p.185)}).
Ce dernier publie un premier ouvrage d'astronomie en 1498,
dédié au Pape Alexandre VI,
intitulé \emph{Lumen caeli, sive Expositio instrumenti astronomici a se excogitate}.
Imprimé à Rome par Johannes Besicken, il explique comme utiliser des équatoires\footnote{instruments dédiés au calcul de la position des planètes}, notamment pour produire
des prédictions astrologiques pour la papauté d'Alexandre VI.

\index{Isabelle la Catholique}
\index{Alfonsó de Córdoba}
\index{Alexandre VI}
Le Pape Alexandre VI décède dans des circonstances suspectes le 6 août 1503.
Le Cardinal Francesco Piccolomini élu au conclave du 23 septembre,
et décède le 18 octobre suivant.
Le 1 novembre, le Cardinal Giuliano Della Rovere accède enfin à la fonction pontificale,
sous le nom de Jules II.
Le 15 décembre de la même année\footnote{Si on se fie à la date indiquée dans le colophon},
Peter Liechtenstein fait sortir un ouvrage de ses presses,
qui rassemble des tables compilées par Alfonsó de Córdoba
en hommage aux \og rois catholiques\fg{}, avec le titre \emph{Tabule Elisabeth Regine}.
On peut notamment en trouver un exemplaire à la
Bibliothek der Eidgenössische Technische Hochschule (\citeprocitem{2}{4435, 1503}),
qui a été numérisé.
Ces tables proposent une alternative aux tables alphonsines,
avec une présentation légèrement différente,
compilée pour l'ère du règne de la Reine Catholique,
et sans les tables dédiées aux éclipses.
Pour une analyse détaillée de leur contenu, voir (\citeprocitem{4}{Chabás, 2004}).

Après la publication des tables de la reine Isabelle par Peter Liechtenstein en 1503,
on peut également noter en 1511 l'édition \emph{princeps}, à Francfort,
d'un autre ensemble de tables
dérivé des tables alphonsines: les \emph{Tabule resolute},
dont la tradition manuscrite remonte à la première moitié
du \textsc{xv}e siècle\footnote{Voir (\citeprocitem{3}{Chabás, 2019} p.311-320)}.
\section{Tables alphonsines de 1521}
\label{sec:org7c9874e}
Cette période est également marquée en astronomie
par la publication de premiers commentaires sur le traité
de théorie planétaire de Georg Peurbach.
Cette nouvelle phase dans la circulation de l'ouvrage
donne lieu à de nouvelles manières d'approcher
l'édition des livres d'astronomie.
Isabelle Pantin montre que dans les éditions des \emph{Theorice nove}
qui sont produites entre 1495 et 1512,
la taille des diagrammes est réduite.
Les diagrammes s'insèrent directement dans le texte
pour avoir une place déterminée dans le flot de lecture
(\citeprocitem{6}{Pantin, 2012}).
Précédemment, le texte et le diagramme étaient en interaction,
le diagramme fournissant les éléments sur lesquels
pouvait porter la démonstration géométrique.
Les deux ne pouvaient pas être séparés, et on invitait le lecteur
à faire passer régulièrement son attention de l'un à l'autre.
Dans les éditions produites à ce moment de transition
entre la période incunable et post-incunable,
on semble assister à une évolution de cette conception:
la lecture du diagramme intervient à un moment précis dans la lecture
pour figurer la configuration géométrique dont le texte fait mention.

En 1518 ou 1521\footnote{La page de garde donne l'année 1518 tandis que le colophon
indique 1521.},
Peter Liechtenstein propose des tables alphonsines.
Je crois que l'on peut voir un peu ce nouvel esprit
qui se manifeste dans le choix de la mise en page de cet ouvrage.
Un exemplaire se trouve à la Biblioteca de la Universidad Complutense de Madrid,
et a été numérisé par Google (\citeprocitem{1}{21018, 1518}).
L'ouvrage est imprimé en caractères germaniques et à l'encre noir
sur du papier au format \emph{in-quatro}.
Il comporte seize cahiers
avec une signature sur les quelques premiers folios de chaque cahier:
quatorze cahiers de huit folio (A-C et F-Q)
avec deux cahiers de quatre au milieu (D et E).
Le numéro des folios est imprimé en chiffres arabes
en haut de chaque recto.

C'est le premier ouvrage du corpus étudié dans ce mémoire
qui comporte une page entièrement dédiée au titre, sur laquelle on peut lire:
\begin{quote}
\og Tabule Astronomice Divi Alfonsi Regis Romanorum et Castelle:
Nuper quem diligentissime cum additionibus emendate
Ex officina litteria Petri Liechtenstein.\\
Anno 1518. Venetis.\\
Cum privilegio.\fg{}
(\citeprocitem{1}{21018, 1518} f.A1r)
\end{quote}
Au verso, on trouve une table des matières
qui indique, pour chaque table,
le folio de début de la table et celui du canon correspondant.

À partir du folio A2r, on trouve les canons de Johannes Lucilius Santritter,
qui sont précédés du même titre que dans l'édition de 1492.
Chaque nouveau canon est introduit par le numéro de la proposition,
inscrit à la fois en toutes lettres et en chiffre arabe,
et commence par une lettrine décorée.

Chaque fois que le canon fait référence à une table,
l'imprimeur a ajouté dans la marge, à gauche ou à droite,
le numéro du folio de la table correspondante.
Sur l'exemplaire de la Biblioteca de la Universidad Complutense,
on peut également voir des notes manuscrites
corrigeant certains oublis dans ces renvois.

Le paragraphe qui concluait les canons dans l'édition de 1492,
et qui introduisait les tables du cahier e (voir chapitre \ref{chap:santritter})
ne sont pas reproduits dans l'édition de 1521.
À la place, les canons des tables du cahier e sont simplement ajoutés
dans la continuité des autres.
Cependant, les propositions de ces canons supplémentaires ne sont pas numérotées
et sont introduites par un titre marqué par un capitulum.
Ces titres reprennent ceux qui étaient déjà présents dans l'édition de 1492.

Les canons se terminent alors en haut du folio D3r,
avec un simple \og Finis\fg{}.
Les tables commencent juste en-dessous
avec la table des lieux géographiques, identique à celle de l'édition de 1492.
Une simple ligne horizontale avec un astérisme séparent les canons des tables.

Les tables s'enchaînent ensuite, presque exactement dans le même ordre
et avec les mêmes paramètres que dans l'édition de 1492.
La seule différence notable concerne la table pour les lieux géographiques,
qui est donnée en deux versions dans l'édition de 1521:
on trouve d'abord une table dans laquelle les longitudes sont données
en heures et minutes, comme dans l'édition de 1492,
puis une table dans laquelle les longitudes sont données en degrés et minutes,
comme dans l'édition \emph{princeps}.

En symétrique de ce que l'on vient de dire dans les canons,
on trouve dans la marge autour des tables
le numéro des canons qui décrivent l'usage de la table.

Enfin, après la table des lieux géographiques,
on peut trouver un explicit qui se lit:
\begin{quote}
Expliciunt canones sine propositiones in tabulas Alfonsi:
clarorum Germanorum Joannis Saxoniensis: et Joannis Lucilii Santritter Heilbronnensis.
(\citeprocitem{1}{21018, 1518} f.D4v)
\end{quote}
\section{Conclusion}
\label{sec:orgee0911d}
Ce que l'on peut tirer de ces remarques,
c'est que l'astronomie alphonsine continue de se développer
au début du \textsc{xvi}e siècle.
D'une part, elle semble toujours être enseignée à l'université.
En effet, au vu des ouvrages produites par Peter Liechtenstein,
le marché étudiant et universitaire est un marché qui l'intéresse
au moment où édite ces tables.
D'autre part, l'astronomie alphonsine intéresse des médecins et astrologues,
comme peut-être d'autres corps de métiers.

Suivant l'évolution des modes typographiques,
le genre des ouvrages d'astronomie alphonsine évoluent également.
Ils s'écartent du modèle des manuels scolaires destinés à l'enseignement oral
et donnent des éléments pour diriger l'attention du lecteur ou de la lectrice.

Les ressources disponibles pour pratiquer l'astronomie se diversifient également.
En plus des tables qui revendiquent explicitement leur lien
avec la figure d'Alphonse X de Castille
et avec la tradition \og alphonsine\fg{},
on trouve d'autres ensembles dérivés,
comme les \emph{Tabule resolute} ou les tables de la reine catholique.
Ces tables offrent d'autres manières d'approcher les calculs alphonsins,
mais partagent les mêmes paramètres et produisent des résultats sensiblement similaires.

Pour autant, l'édition des tables semble être motivée par une ambition différente
de celle de Johannes Lucilius Santritter en 1492.
En effet, les éditeurs qui produisent ces nouvelles versions des tables
ne sont pas aussi investis que le mathématicien de Heilbronn dans l'amélioration
des modèles de l'astronomie et de son enseignement.
Les enjeux mathématiques et scientifiques qui semblaient chers aux artisans
de l'édition de 1492 ont été mis au second plan derrière de nouvelles préoccupations.

D'abord, on commence à voir se développer une astronomie de cour,
dans laquelle un des enjeux pour les astronomes
est de promouvoir l'influence de leur patron,
dans le contexte instable généré par les guerres d'Italie.

Ensuite, on voit s'installer un peu partout en Europe un système de privilèges et de censure,
qui sont adoptés à Venise à cette époque pour réguler le développement de l'imprimerie
(\citeprocitem{9}{Kikuchi, 2016}).
Dans ce cadre, un imprimeur allemand à Venise a besoin de s'attirer le soutien
de personnes influentes, capables d'intercéder en sa faveur pour obtenir
les privilèges dont il a besoin pour exercer son métier.
Le nom associé aux tables devient alors un enjeu presque aussi important
que la qualité des paramètres et des théories qui sont mises en jeu
dans leur compilation.

\section{Bibliographie}
\label{sec:orgc25f309}
\subsection{Sources primaires}
\label{sec:org04c55e3}
\hypertarget{citeproc_bib_item_1}{[1] BH FLL 21018, \textit{Tabule astronomice diui Alfonsi Regis Romanorum et Castille}. (1518) Peter Liechtenstein, Venise. (en ligne: \url{https://books.google.fr/books/ucm?vid=UCM5325125720&redir_esc=y}).\newline Madrid, Biblioteca de la Universidad Complutense de Madrid, numérisé par Google.}

\hypertarget{citeproc_bib_item_2}{[2] Rar 4435, \textit{Tabule astronomice Elisabeth Regine}. (1503) Peter Liechtenstein, Venise. (en ligne: \url{https://www.e-rara.ch/zut/doi/10.3931/e-rara-1987}).\newline Zürich, Bibliothek der Eidgenössische Technische Hochschule.}
\subsection{Littérature secondaire}
\label{sec:orgd1c1a9a}
\hypertarget{citeproc_bib_item_3}{[3] José Chabás, \textit{Computational Astronomy in the Middle Ages: Sets of Astronomical Tables in Latin}. (2019) Consejo superior de investigaciones cieníficas, Madrid (Espagne). Collection « Estudios sobre la ciencia ». ISBN 978-84-00-10558-7.}

\hypertarget{citeproc_bib_item_4}{[4] José Chabás, « Astronomy for the Court in the Early Sixteenth Century: Alfonso de Córdoba and his Tabule Astronomice Elisabeth Regine ». in. \textit{Archive for history of exact sciences},. vol. 58,. nᵒ 3. p. 183‑217. 2004. (\url{http://dx.doi.org/10.1007/s00407-003-0073-2}).}

\hypertarget{citeproc_bib_item_5}{[5] Angela Nuovo, « The Book Trade in the Italian Renaissance ». in. vol. 29,. 2013. (\url{http://dx.doi.org/10.1163/9789004208490}).}

\hypertarget{citeproc_bib_item_6}{[6] Isabelle Pantin, « The First Phases of the Theoricae Planetarum Printed Tradition (1474-1535). The Evolution of a Genre Observed through Its Images ». in. \textit{Journal for the history of astronomy},. vol. 43,. p. 3‑26. 2012.}

\hypertarget{citeproc_bib_item_7}{[7] Joseph Pérez, \textit{Isabelle la Catholique: Un modèle de chrétienté?} (2004) Payot, Paris (France). ISBN 2-228-89908-9.}

\hypertarget{citeproc_bib_item_8}{[8] Joseph Pérez, \textit{Isabelle et Ferdinand: Rois Catholiques d’Espagne}. (1988) Fayard, Paris (France). ISBN 2-213-02110-4.}
\end{document}
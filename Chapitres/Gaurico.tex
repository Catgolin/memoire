% Created 2023-05-26 ven. 21:14
% Intended LaTeX compiler: pdflatex
\documentclass[12pt]{article}
\usepackage[LAE, LGR, T2A, T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage[french]{babel}
\usepackage{setspace}
\usepackage{imakeidx}
\usepackage{textalpha}
\usepackage{arabtex}
\usepackage{utf8}
\usepackage[newfloat]{minted}
\usepackage[hidelinks]{hyperref}
\usepackage{geometry}
\onehalfspacing
\usepackage{paracol}
\geometry{a4paper, total={6.5in, 10in}}
\footnotelayout{m}
\makeindex[intoc]
\author{Clément Cartier\thanks{contact@catgolin.eu}}
\date{\today}
\title{Les tables du Roi d'Espagne revisitées par Luca Gaurico (1524)}
\hypersetup{
 pdfauthor={Clément Cartier},
 pdftitle={Les tables du Roi d'Espagne revisitées par Luca Gaurico (1524)},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.6-pre)}, 
 pdflang={French}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\maketitle
\tableofcontents
\newpage
\section{Luca Gaurico: un astrologue au cœur des guerres d'Italie}
\label{sec:org38bb005}
Luca Gaurico est, avec Nostradamus (1503-1566),
l'un des astrologues les plus célèbres du début du \textsc{xvi}e siècle\footnote{Pour la biographie
de Luca Gaurico, je me base principalement sur l'article du
\emph{Dizionario Biografico degli Italiani} (\citeprocitem{9}{di Franco, 1999})}.
Né en 1475 à Gauro, il entre à l'université de Padoue à la mort de son père, en 1497.
Il y fréquente les cercles humanistes, et est diplômé de médecine probablement entre 1503 et 1504.
À cette période, il travaille avec des imprimeurs Vénitiens, notamment Luc'antonio Giunta,
pour l'édition de textes scientifiques, et donne des cours de mathématiques et d'astronomie.
Entre 1503 et 1504, il commence également à publier des pronostiques astrologiques
dans lesquels il prédit des cataclysmes et de profondes transformations dans l'Église,
qu'il dédie à différentes personnalités du camps des Sforza.

À partir de 1506, il commence à enseigner l'astronomie à l'université de Bologne.
Là, s'oppose à Giovanni Bentivoglio (1443-1508), premier citoyen de Bologne:
à cette époque, la commune de Bologne était une cité autonome, placée sous la protection du Pape
et la famille Bentivoglio y déployait son autorité avec le soutien des ducs de Milan (les Sforza)
et des seigneurs de Toscane (les Médicis).
Après le décès d'Alexandre VI, le Pape Jules II s'allie à la France dans la guerre contre Milan,
les Médicis se rangent du côté de la France,
et Giovanni Bentivoglio, toujours allié au camps Sforza,
tente de faire éliminer leurs soutiens à Bologne.
Dans ce contexte, Luca Gaurico prédit le malheur des Bentivoglio.
Il est emprisonné et torturé, puis relâché grâce à l'aide de ses amis.
Peu de temps après, le Pape marche sur Bologne avec l'aide des troupes Françaises
et excommunie Giovanni Bentivoglio.
En 1507, alors que le Pape est toujours à Bologne,
Luca Gaurico lui dédie de nouvelles prédictions.
Il prédit notamment l'arrivée de faux prophètes en 1530, et un déluge en 1524
(l'année de la publication de son édition des tables alphonsines)
à cause de la conjonction des planètes dans le signe du Poisson.

Après ces événements, Luca Gaurico gagne l'université de Ferrare en 1507.
La ville est alors sous le contrôle de la famille Médicis, alliée à la France.
Là, il donne des cours sur l'astronomie de Ptolémée, publiés à Venise par Luc'antonio Giunta.
Il s'engage également dans de nombreuses disputes au sujet de la légitimité
de l'astrologie, et sur l'histoire de la discipline.

Il retourne à Bologne en 1509, où il dédie des pronostiques pour le Marquis de Mantoue,
François II Gonzaga (1466-1516).
Ce dernier se prépare alors à attaquer Venise pour le compte de la ligue de Cambrai,
qui rassemble la France, le Saint-Empire et le Pape contre la République de Venise.
Luca Gaurico prédit le succès du marquis, qui est emprisonné à Venise l'année suivante
avant d'être libéré en échange de son fils en 1510.
Entre 1509 et 1511, l'astrologue semble voyager entre Rome et Bologne,
et continue de publier de nouvelles prédictions chaque année.
Mais en 1511, il est agressé par Annibale II Bentivolio (1467-1540),
le fils de Giovanni Bentivoglio, qui s'allie à la France pour retrouver le pouvoir à Bologne.

Luca Gaurico trouve alors refuge auprès de François II Gonzaga,
et reste à Mantoue jusqu'en 1513.
Les problèmes ne s'arrêtent pas pour autant pour lui,
car beaucoup de ses prédictions pour les années 1511-1512,
produites plusieurs années plus tôt,
ne se produisent pas.
Pour se défendre, il accuse notamment les imprimeurs Vénitiens
d'avoir imprimé de faux pronostics à son nom,
et réaffirme l'imminence de catastrophes en 1524 ainsi que l'émergence de \og faux prophètes\fg{}
en 1530 et la réforme des Églises en 1535.

Bacchelli di Franco note un trou dans les sources au sujet de Luca Gaurico après 1513,
mais tout indique qu'il s'est trouvé à Rome, probablement sous la protection du Cardinal
Jean de Médicis (1475-1521), devenu Pape Léon X le 11 mars 1513.
Durant cette période, le roi de France, François Ie de la maison des Valois d'Angoulême
(1494-1547) signe avec le Pape le Concordat de Bologne en 1515, qui déclenche la conclusion
de la cinquième guerre d'Italie.
Mais le fils issu de l'alliance entre les enfants de la reine Isabelle la Catholique
et de l'empereur Maximilien de Habsbourg (voir chapitre \ref{chap:liechtenstein}),
Charles II de Habsbourg (1500-1558)\footnote{futur Charles V du Saint-Empire,
et plus connu sous le nom de Charles Quint} revendique son autorité
sur tous les royaumes qui avaient été réunis par ses grand-parents,
dont le royaume de Naples et les nouvelles possessions Espagnoles sur le Nouveau continent,
en plus de ses revendications sur la couronne du Saint-Empire et sur le duché de Bourgogne\footnote{Il prétend hériter du duché de Bourgogne grâce à l'alliance de son grand-père, Maximilien,
avec Marie de Bourgogne (1457-1482),
dernière héritière de la maison des Valois de Bourgogne:
il est ainsi le petit-cousin de François Ier}.
Léon X tente de limiter les prétentions de Charles II, mais doit également faire face
aux partisans de Martin Luther.
À la mort de Maximilien en 1519, Charles II est élu face au roi de France, François Ier,
soutenu par Léon X, et Charles Quint est couronné en 1520.
L'empereur s'allie alors avec les Sforza pour reprendre le duché de Milan à la France,
et reçoit le soutien du Pape dans cette entreprise en échange de son aide
dans la lutte contre Martin Luther et contre les attaques Ottomanes.
Cependant, en 1521, le roi de France s'associe à la République de Venise
pour contester l'influence de Charles de Habsbourg sur le royaume de Navarre.
Léon X décède en décembre de la même année.
Le Cardinal Adriaan Boeyens, précepteur de Charles II, accède alors au Saint-Siège sous le nom
d'Adrien VI en 1522, sans réels soutiens au sein de la Curie.
Il décède en 1523, et un nouveau Pape doit être élu.
Le Cardinal Julien de Médicis, cousin de Léon X, est élu après un long conclave
grâce au soutien de Charles Quint, le 19 novembre 1523.

C'est dans ce contexte particulièrement électrique
que Luca Gaurico voit arriver l'année 1524,
pour laquelle il prédit depuis une dizaine d'année
d'importants cataclysmes.
Dans la préface de l'édition des tables alphonsines qu'il publie cette année là,
et qui est l'objet de ce chapitre,
il nous informe qu'il a pris refuge à Venise,
et qu'il correspond avec l'évêque d'Arezzo, Ottaviano Sforza (1475-1545),
fils de l'ancien duc de Milan Galeazzo Sforza (1444-1476),
et cousin de Francesco Maria Sforza (1495-1535)
à qui Charles Quint a promis de rendre le Duché de Milan.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../images/maps/life_gaurico.png}
\caption{Carte des principaux lieux de vie de Luca Gaurico}
\end{figure}

Ainsi, Luca Gaurico semble être dans une posture difficile:
il a des ennemis à la fois dans le camps des Habsbourg,
au travers de la famille Bentivoglio,
et il prend clairement position contre le Roi de France.
Ses soutiens à Rome sont également incertains: Clément VII est élu avec le soutien
des partisans des Habsbourg, mais les Médicis combattent aux côtés de la France.
Par ailleurs, la réputation de l'astrologue est sérieusement mise en jeu
par ses prédictions pour l'année 1524.
Il affirme que c'est en raison des cataclysmes prévus pour cette année qu'il a quitté Rome
pour gagner Venise\footnote{\og Cuom anno salutatoris quarto et vigesimo supra sesquimillesimum
horrifica pestis formidine tremebundus
ab tua urbe secederem
ad Venetos dii quos posuere lares.
Octaviani Sfortiade episcopi Aretini suasu.\fg{}\\
\og En l'année du Salut mille cinq cent vingt quatre,
tremblant de peur d'une horrible catastrophe,
je me retirai de votre ville
pour gagner Venise sur laquelle la protection de Dieu est placée.\fg{}}.
Il est probable également que sa situation à Venise ne soit pas très stable:
en 1524, Venise est alliée avec les Habsbourg,
mais de nombreux notables soutiennent le parti français,
et surtout Luca Gaurico a beaucoup critiqué les imprimeurs Vénitiens
pour justifier de la fausseté des pronostics qui lui étaient attribués.

Mais il continue de se défendre, en faisant éditer de nombreux textes
d'astronomie et d'astrologie chez Luc'antonio Guinta.
Il prédit notamment la défaite rapide du camps Français,
alors que depuis quelques mois, les armées de François Ier
se sont sortis d'une situation difficile en Provence,
et commencent à menacer les positions du Duché de Milan
en s'attaquant à Pavie\footnote{De fait, en 1525, le siège de Pavie
tourne rapidement au désastre, François Ier est fait prisonnier
et doit faire de nombreuses concessions pour négocier sa libération.}.

C'est dans ce contexte qu'il publie sa version des tables alphonsines,
qu'il dédie au Pape Clément VII.
\section{L'édition de 1524 par Luc'antonio Giunta}
\label{sec:orgc103caf}
\subsection{Présentation de l'ouvrage}
\label{sec:org4b4882f}
L'ouvrage imprimé par Luc'antonio Giunta en 1524
subsiste en de nombreux exemplaires.
J'ai utilisé comme référence principale celui qui est
préservé à la bibliothèque de l'Observatorio astrofisico di Torino
(\citeprocitem{3}{2766.401v, 1524}).
Il s'agit d'un ouvrage en papier avec des caractères gothiques,
au format in-quatro.
Il se divise en deux parties reliées ensemble:
d'abord, seize cahiers signés avec des lettres majuscules
contiennent les tables alphonsines. Les quinze premiers cahiers (A-P)
sont des cahiers de huit folio, et le cahier final (Q) en comporte six.
La deuxième partie présente les tables de la Reine catholique,
avec quatre cahiers signés en lettres minuscules: trois cahiers de huit folios
(a-c) et un cahier final de quatre folios (d).
Certains folios sont numérotés en haut de la page en chiffres arabes.

La première page (folio A1r)
commence par un titre imprimé à l'encre rouge,
qui se lit
\begin{quote}
\begin{paracol}{2}
Alfonsi Hispaniarum Regis Tabule et L. Gaurici
Artium doctoris egregii Theoremata quorum hic est index
(\citeprocitem{3}{2766.401v, 1524} f.A1r)
\switchcolumn
Tables du Roi des Espagnes Alphonse
et les excellentes propositions de Luca Gauroco, docteur en arts,
dont voici l'index
\end{paracol}
\end{quote}
suivi par la table des matières imprimée à l'encre noire.
C'est la première fois dans ce corpus que le roi Alphonse X
est présenté comme «roi des Espagnes»,
titre que Charles Quint est le premier à revendiquer.

En dessous de la table des matières,
à l'encre rouge,
on trouve la note
\begin{quote}
\begin{paracol}{2}
Ne quispiam alius calcographius Venetiis aut ausque locorum Venetae ditionis:
impune hac L. Gaurici castigationes:
additione sue imprimat:
Senatus Veneti decreto cautum est\\
(\citeprocitem{3}{2766.401v, 1524} f.A1r)
\switchcolumn
Le décret du Sénat de Vénise avertit
que toute personne qui imprime à Venise ou dans les territoires sous domination Vénitienne
les prédictions de Luca Gaurico sans son accord seront punies.
\end{paracol}
\end{quote}
Cette note est le signe que le principes de privilèges d'imprimeurs
ne suffisent pas à réguler la diffusion d'une œuvre aussi polémique que celle
de Luca Gaurico.

Enfin, en bas de la page, on trouve la marque de l'imprimeur,
une fleur de Lys rouge, symbole des origines Florentines des Giunta,
entourée par la note
\begin{quote}
\begin{paracol}{2}
In calce huius libri seorsum annexe sunt tabulae Elisabeth Regine
nuper castigatae et in ordinem redactae per L. Gauricum cum
additionibus et Novis problematibus eiusdem Gaurici.
(\citeprocitem{3}{2766.401v, 1524} f.A1r)
\switchcolumn
À la fin de ce livre sont annexées séparément
les tables de la Reine Isabelle,
récemment corrigées et mises en ordre par Luca Gaurico
avec des ajouts et des problèmes nouveaux par le même Gaurico.
\end{paracol}
\end{quote}

Le recto du folio A1 est une préface de Luca Gaurico
adressée au Pape Clément VII,
dans laquelle l'astrologue vante les mérites de sa discipline,
utile selon lui dans toutes les professions\footnote{\og Hec siquidem ars divina utilis quidem est grammaticis / Rhetoribus / dialeticis / neccessaria phis / necessaria admodum et medicis.
Agricolis preterea / navium gubernatoribus / architectis imperatoribus.
Nam uti mantuanus cecinit homerus.\fg{}
(\citeprocitem{3}{2766.401v, 1524} f.A1v)\\
\og En effet, cet art divin est bien utile aux grammairiens, aux rhétoriciens, aux dialecticiens, nécessaire aux physiciens, plus nécessaire aux médecins,
plus encore indispensable aux fermiers, aux capitaines de navires, aux architectes et aux emprereurs,
comme le chantait l'Homère de Mantoue [Virgile].\fg{}}.
\subsection{Tables du roi Alphonse}
\label{sec:org34b645b}

\subsubsection{Tables chronologiques, géographiques, et équation du temps}
\label{sec:org87e73cf}
Dès le folio A2r, on trouve la première table,
qui concerne les différences entre les débuts de chaque ère,
depuis la naissance d'Adam jusqu'au règne d'Alphonse X.
Aucun canon n'accompagne cette table,
qui est immédiatement suivie au verso par un canon
qui explique la manière de lire la table des lieux géographiques
et de convertir les longitudes exprimées en degrés en longitudes exprimées en heures.
On trouve ensuite deux tables permettant d'exécuter ces conversions (A2v et A3r).

Sur le folio A3v, on trouve d'abord un court texte
expliquant comment trouver l'heure du midi à partir de la longitude du méridien exprimé
en heures et en minutes, suivi par une table des lieux géographiques
présentée dans un format identique à celui de l'édition de 1492 (voir \ref{chap:santritter}).
En face, sur le folio A4r, on a de la même manière un court texte en haut de la page
qui explique comment utiliser la table de l'équation du jour
pour corriger l'heure du milieu de la journée,
suivi de la table de l'équation du jour juste en-dessous.

La double-page constituée par les folios A4v et A5r
donnent à la fois les canons et les tables nécessaires
pour effectuer les conversions entre les ères.

\subsubsection{Tables sur le mouvement de la huitième sphère}
\label{sec:orgac69bc5}
Toute la page du folio A5v est dédiée aux canons pour le calcul
des mouvements de la huitième sphère.
Les deux faces du folio suivant donnent les tables correspondantes:
moyen mouvement de l'auge et moyen mouvement d'accès et de recès,
avec les racines du mouvement à l'incarnation pour le méridien de Tolède
en haut de la page.

Dans l'exemplaire de l'Osservatorio astrofisico di Torino
(\citeprocitem{3}{2766.401v, 1524}),
on ne trouve pas de table pour l'équation du mouvement de la huitième sphère.
Sur le folio A7r, on trouve à la place
un tableau qui donne un exemple de calcul réalisé à partir des tables.
Dans d'autres témoins (\citeprocitem{1}{180, 1524}, \citeprocitem{2}{20989, 1524}),
cet exemple n'est pas présent et le folio A7r
est dédié à la table de l'équation.

C'est à-peu-près ainsi que s'organise toute la suite de l'ouvrage:
les canons sont immédiatement suivis par les tables qui leur sont associés,
puis on trouve parfois un exemple de calcul effectué à partir des tables.

Pour les mouvements de la huitième sphère, Luca Gaurico
propose également une méthode alternative.
Avec les tables précédentes, pour calcul les mouvements de la huitième sphère,
il faut
\begin{enumerate}
\item d'une part calculer le mouvement moyen des étoiles fixes
et des apogées des planètes,
\item d'autre part calculer le mouvement d'accès et de recès de la huitième sphère,
ce qui nécessite
\begin{enumerate}
\item d'abord, de calculer son mouvement moyen
\item puis, d'appliquer l'équation du mouvement à ce mouvement moyen.
\end{enumerate}
\end{enumerate}
Le processus nécessite donc l'utilisation de trois tables différentes\footnote{Voir
(\citeprocitem{7}{Chabàs \& Goldsein, 2012} pp.48-51)}.
En 1495, Simon Bevilaqua avait imprimé à Venise une édition des tables astronomiques
compilées par Giovanni Bianchini, un an avant la mort de ce dernier en 1496\footnote{La composition des tables de Bianchini est cependant bien antérieure
à son édition \emph{princeps}: plusieurs manuscrits nous renseignent sur le fait
qu'elles ont été copiées à Cracovie en 1488, et Regiomontanus les aurait copiées
pour lui-même en 1460 (\citeprocitem{8}{Chabás \& Goldstein, 2009} pp.13-15)}.
Dans cette édition, on trouve notamment une table
dédiée à simplifier le calcul des mouvements de la huitième sphère\footnote{Voir
figure \ref{fig:org1386bba}}.
La table proposée par Bianchini permet de lire directement le résultat du calcul,
sans avoir à manipuler des tables intermédiaires\footnote{Pour plus de détails
sur le fonctionnement des tables de Bianchini sur la précession,
voir (\citeprocitem{8}{Chabás \& Goldstein, 2009} pp.28-34)}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{/home/catgolin/.cache/org-persist/09/2511bb-64f0-466a-8cc5-a9568d5610e2-4d5a241d4f01b09464377da1c836777f.jpg}
\caption{\label{fig:org1386bba}Table du mouvement de l'auge commune issue de (\citeprocitem{5}{4 Inc c.a. 1185, 1495} pp.61-70/f.a2r-a6v)}
\end{figure}

Dans l'édition de Lucantonio Giunta de 1524,
comme dans sa réédition par Chrétien Wechel en 1545,
on peut trouver, à la suite des trois tables alphonsines du calcul de l'accès
et du recès de la huitième sphère,
un exemplaire de la table de Bianchini.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{/home/catgolin/.cache/org-persist/c8/692118-135c-4105-a40a-fbddc7d8a274-a16a0ac569855d05c949656eea97eb0a.jpg}
\caption{\label{fig:orgcaf4ee5}Table du mouvement de l'auge commune issue de (\citeprocitem{4}{4 Eph. astr. 3, 1545} pp.25-26/f.B3r-v)}
\end{figure}

\subsubsection{Tables planétaires}
\label{sec:orgcc170a2}
Après les tables de Bianchini,
on trouve une table de multiplication qui s'étend
sur les folios B1r à B6v,
et qui reproduit celle que Johannes Lucilius Santritter
avait ajoutée à la fin des tables dédiées
au calcul de la position des planètes
(voir chapitre \ref{chap:santritter}).

Le folio B7r donne ensuite les canons pour déterminer
les mouvement du Soleil, avec un exemple.
Il est suivi par la table du moyen mouvement du Soleil (B7v)
et celle de l'équation (B8r-C1r).

On trouve ensuite le même genre de canon pour la Lune (folio C2v),
avec un exemple (folio C3r),
puis les tables correspondantes:
moyens mouvements de la longitude (f.C3r) et de l'anomalie (f.C3v),
équations (C4r-C6v), position des nœuds (C7r)
et table de latitude (C7v).
À chaque fois, la racine du mouvement pour l'ère de l'Incarnation
est donnée en haut de la table pour le méridien de Tolède.

Ensuite, Luca Gaurico donne les canons pour calculer la position des planètes
en longitude uniquement (pas en latitude)
sur le folio C8r,
et tous les folios des cahiers D et E sont consacrés aux tables
de moyen mouvement et d'équations des planètes inférieures et supérieures,
jusqu'au folio F1v.

Sur les folios F2r à F4r,
on trouve des remarques sur le calcul du vrai lieu des planètes
ainsi que les canons pour le calcul des latitudes, visibilité et stations
de la planète, avec quelques exemples.
Les folios F4v à F6v donnent ensuite les tables de \og passions\fg{} des planètes,
à l'identique de celles de l'édition de Peter Liechtenstein
(voir chapitre \ref{chap:liechtenstein}), elles-mêmes copiées
sur l'édition de 1492 (voir chapitre \ref{chap:santritter}).

\subsubsection{Tables d'horoscope et de calendrier}
\label{sec:orgc8efb61}
Le folio F6r décrit la procédure pour composer un horoscope
à partir de la position du Soleil.
Il est suivi par une table d'ascension droite
qui complète la fin du cahier F, et les tables d'ascension oblique
pour chaque climat sur l'ensemble du cahier G et jusqu'au recto du folio H3.
Sur les folios H3v à H4v, on trouve deux tables
absentes des autres éditions qui permettent de calculer le vrai lieu du Soleil
pour chaque jour de l'année.

Sur le folio H5, on apprend comment calculer
la date d'entrée du Soleil dans chaque signe,
avec les canons au recto et la table au verso.

On trouve également sur le dernier folio du cahier H
une table qui donne, pour chaque année,
la date de la conjonction vraie du Soleil et de la Lune.

Folio I3r, on trouve une table qui donne la planète
célébrée le premier jour de chaque mois, pour chaque année
depuis 1512 jusqu'en 1540,
suivi au verso par une table permettant de compléter
un calendrier.

Les reste du cahier I et le premier folio du cahier K
sont dédiés aux calculs de la conjonction entre les planètes,
avec des tables déjà présentes dans les précédentes éditions
des tables alphonsines.

\subsubsection{Éclipses}
\label{sec:org1421a21}
À partir du folio K2 inclus, tout le cahier K est dédié
au calcul des conjonctions et oppositions
entre les deux luminaires.

Le cahier L est presque entièrement dédié aux canons
pour expliquer le calcul des éclipses, avec les tables
qui s'insèrent à la suite du canon,
et qui sétendent jusqu'au folio M5r.

Au folio M5v, Luca Gaurico propose des méthodes
pour calculer les oppositions et conjonctions des luminaires
pour chaque mois.
Il donne un exemple en face,
sur le folio M6r, pour l'année 1527.
Au verso du folio M6, il donne ensuite
le moment et le lieu (en longitude et en argument)
de l'opposition moyenne des deux luminaires pour chaque 40 années,
depuis l'année 40 jusqu'à l'an 2000,
puis des tables qui s'étendent jusqu'au folio N6r
pour calculer la position des conjonctions et oppositions entre les luminaires.

Sur la double-page entre le folio N6v et N7r, on trouve la table du mouvement horaire
du Soleil, déjà présente dans les autres éditions,
puis sur la double-page suivante, une liste des dates
des éclipses de Soleil et de Lune à Rome, entre 1525 et 1573.

\subsubsection{Position des astres}
\label{sec:org63dbc42}
Sur la dernière page du cahier N,
Luca Gaurico explique à l'adresse du Pape l'intérêt
de calculer la position des étoiles pour prédire le cours des événements
à différentes échelles de pouvoir.
J'en reproduit un extrait ci-dessous:
\begin{quote}
\begin{paracol}{2}
Octavi Sfortiadae Episcopi Aretini, olim Laudensis,
suasu labente anno Christianae salutis 1500 supputavimus,
et ad libellam examinavimus atque rectificavimus in finitore Venetiano 1027 stellas fixas secundum Ptolemeaeum.
Quas prisci Arabes, Chaldæi seu Babyloni, dein Hyparcus, Ptolemæus,
et iam tandem Alfonsus Hispaniarum Rex inclytissimus tum in longitudine tum latitudine observarunt,
eisque nomina et ex diuturna observatione planetarias qualitates contribuerunt et in sex ordines distribuerunt atque,
distinxerunt.
\switchcolumn
Sur la suggestion d'Ottaviano Sforza, évêque d'Arezzo\footnote{Ottaviano Sforza
est nommé évêque d'Arezzo en 1519 par le Pape Léon X. Il démissionne de cette fonction
en 1425, après la défaite française à la bataille de Pavie qui fait du chateau des Sforza
à Milan un point stratégique de la guerre d'Italie.}, précédemment de Lodi\footnote{Ottaviano
Sforza a été nommé évêque de Lodi pour la première fois en 1497 par le Pape Alexandre VI,
puis est révoqué en 1499 après la conquête de Milan par Louis XII.
Il est rétabli à cette position en 1512 par le Pape Jules II après les défaites françaises
dans la guerre contre la Sainte Ligue.
Il quitte Lodi en 1519 pour gagner le diocèse d'Arezzo,
Après avoir participé à la reconquête de Milan en 1527, il retrouve à nouveau
le titre d'évêque de Lodi, qu'il garde jusqu'en 1530.
Il est ensuite nommé Patriarche latin d'Alexandrie et évêque de Latina-Terracina-Sezze et Priverno
en 1540 ou 1541 par le Pape Paul III, positions qu'il occupe jusqu'à sa mort en 1545.},
nous avons calculé pour l'année du Salut chrétien 1500, examiné et rectifié les noms
des 1027 étoiles fixes dans les frontières Vénitiennes selon Ptolémée,
que les anciens Arabes, Chaldéens ou Babyloniens,
puis Hipparque , Ptolémée et enfin le Alphonse, le plus célèbre roi d'Espagne,
observaient à la fois en longitude et en latitude et dont ils ont apporté
les noms et les les qualités planétaires à partir d'observations nocturnes,
et ont réparties en six ordres.

\switchcolumn[0]*
Quippe qui primae magintudinis ac luminis, quæ regiæ et augustæ sunt cognominatæ,
pontificam illam atque,
cardineam vestram maiestatem præ se ferre videntur.
Quæ secundi splendoris, cæsaream coronam,
quae fulgoris tertii, reges;
quarti autem ordinis, magnanimos duces ac principes;
quinti, patricios atque urbium primates;
sexti denique, plebeculam.\\
(\citeprocitem{3}{2766.401v, 1524} f.N8v)
\switchcolumn
Ainsi ceux de la première grandeur,
qu'on appelle royaux et augustes, semblent porter devant eux ce pontificat
et votre majesté cardinale;
ceux de la deuxième splendeur, la couronne de César;
ceux du troisième éclat, les rois;
et du quatrième ordre, les chefs et les princes magnanimes;
le cinquième, les patriciens et les primats des villes;
sixièmement enfin, le peuple.
\end{paracol}
\end{quote}

En face, la première page du cahier O
donne le mouvement de l'auge pour plusieurs années depuis l'an 1
jusqu'à 2004.
Ensuite, jusqu'à la fin du cahier Q,
il s'agit d'un catalogue d'étoiles,
avec un colophon accompagné de la marque d'imprimeur
sur le recto du dernier folio (voir figure \ref{fig:org96a4bb2}).

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../images/illustrations/colophon_alphonse.png}
\caption{\label{fig:org96a4bb2}Colophon de Luc'antonio Giunta à la fin du cahier Q (\citeprocitem{3}{2766.401v, 1524} f.Q6r)}
\end{figure}
\subsection{Tables de la reine Isabelle}
\label{sec:org21bf4c2}
Après les tables du \og roi des Espagnes\fg{},
on trouve un deuxième ouvrage.
La page de titre, sur le folio a1r,
est structurée, comme celle du folio A1r,
avec un titre, une table des matières, une indication du privilège accordé à l'imprimeur,
et la marque de ce dernier.
Contrairement au folio A1r, tout est imprimé en encre noire.

On trouve ensuite les tables de la \og reine Élisabeth\fg{},
similaires à celles imprimées par Peter Liechtenstein en 1503
(voir chapitre \ref{chap:liechtenstein}),
mais où les canons ont été insérés à côté des tables.
Pour une comparaison entre les deux éditions des tables de la \og reine des Espagnes\fg{},
on peut se référer à l'article de José Chabás qui leur est dédié (\citeprocitem{6}{Chabás, 2004}).
\section{Conclusions}
\label{sec:org4904325}
Avec les premières éditions des tables alphonsines,
nous avons vu qu'Erhard Ratdolt et surtout Johannes Lucilius Santritter
cherchaient à transmettre une version des tables alphonsines
améliorée par les nouvelles connaissances de leur temps:
notamment l'imprimerie et les théories de Georg Peurbach,
Giovanni Bianchini et Regiomontanus.
L'exemple de Luca Gaurico montre que les astronomes
de la première moitié du \textsc{xvi}e siècle
ont reçu cet héritage avec un certain enthousiasme,
mais l'ont réinvesti pour servir de nouveaux dessins.

Impacté par les crises qui traversent l'Église
et par les tensions politiques en Europe,
Luca Gaurico cherche plus à justifier la légitimité de son art,
et à prouver sa compétence,
qu'à améliorer les paramètres des tables.
En rapprochant les tables de leurs canons,
en ajoutant de nombreux exemples,
et en expliquant les applications de ces calculs à l'astrologie,
il fait preuve d'une certaine pédagogie.
C'est probablement ce qui fait le succès de cette édition,
et qui justifie sa réédition en 1545 (voir chapitre \ref{chap:wechel}).
Mais cet effort de pédagogie ne semble pas dirigé dans un esprit de faciliter
l'enseignement des tables et de l'astronomie, mais plutôt vers un effort
de convaincre son lectorat.
Son édition est moins un manuel, qu'un tutoriel pour montrer pas à pas
comment retrouver les mêmes conclusions que l'auteur.

Par ailleurs, Luca Gaurico déploie beaucoup d'efforts
pour montrer sa propre compétence.
Il démontre qu'il a lui-même acquis la connaissance
des tables du roi Alphonse
en calculant les exemples,
en ajoutant des tables qui n'appartiennent pas aux éditions précédentes,
en ajoutant ses propres tables pour les éclipses, etc.
On peut noter également que Luca Gaurico insiste sur l'importance
des mouvements de la huitième sphère.
L'une des principales différences entre les tables alphonsines
et les \emph{Tabulae resolutae} qui se diffusent largement à cette époque
pour le calcul des éphémérides, c'est que les secondes simplifient le mouvement
de trépidation pour ne donner qu'une table du moyen mouvement de la huitième sphère.
Ici, Luca Gaurico montre que les tables du roi Alphonse tiennent compte du mouvement
complexe de la huitième sphère, mais donne également les tables de Giovanni Bianchini
qui permettent de simplifier considérablement le calcul.
Ainsi, il fait la promotion de sa propre valeur intellectuelle
à un moment où il sait qu'il s'agit de la chose la plus précieuse qu'il puisse vendre
en échange de la protection de ses mécènes.
Pour ce faire, l'association des tables avec la figure
du roi Alphonse et surtout avec celle de la Reine Catholique
est un argument important, que l'auteur n'hésite pas à mettre en valeur.

Enfin, la préface et les nombreuses adresses au Pape,
l'ajout des tables composées pour Isabelle de Castille
et le titre de \og roi des Espagnes\fg{} font aussi de cette édition
un tract politique pour défendre le camps des Habsbourg
et justifier les prédictions de Gaurico de la défaite française.
Alors qu'on assiste à une diversification des tables disponibles pour calculer
les éphémérides à partir du matériel alphonsin,
l'édition des tables \og du roi Alphonse\fg{} est aussi le moyen de transformer des tables astronomiques
en un héritage qui peut être mis au service d'une dynastie ou d'un camps politique.
\section{Bibliographie}
\label{sec:orgf0be693}
\subsection{Sources primaires}
\label{sec:orgd47e2c1}
\hypertarget{citeproc_bib_item_1}{[1] BH MED 180, \textit{Alfonsi Hispaniarum Regis Tabule}. (1524) Lucantonio Giunta, Venise. (en ligne: \url{https://books.google.com/books/ucm?vid=UCM5324310756}).\newline Madrid, Biblioteca de la Universidad Complutense de Madrid, numérisé par Google.}

\hypertarget{citeproc_bib_item_2}{[2] BH FLL 20989, \textit{Alfonsi Hispaniarum Regis Tabule}. (1524) Lucantonio Giunta, Venise. (en ligne: \url{https://books.google.com/books/ucm?vid=UCM5323778334}).\newline Madrid, Biblioteca de la Universidad Complutense de Madrid, numérisé par Google.}

\hypertarget{citeproc_bib_item_3}{[3] PREG OATO 2766.401v, \textit{Alfonsi Hispaniarum Regis Tabule}. (1524) Lucantonio Giunta, Venise. (en ligne: \url{https://www.beniculturali.inaf.it/bookReader/index.html?f=TO0E018454_OATO}).\newline Turin, Osservatorio astrofisico di Torino.}

\hypertarget{citeproc_bib_item_4}{[4] 4 Eph. astr. 3, \textit{Divi Alphonsi Romanorum at Hispaniarum regis}. (1545) Chrétien Wechel, Paris. (en ligne: \url{https://www.digitale-sammlungen.de/en/view/bsb10158943}).\newline Munich, Bayerische Staatsbibliothek.}

\hypertarget{citeproc_bib_item_5}{[5] 4 Inc c.a. 1185, \textit{Tabulae celestium motuum earumque canones. Ed: Augustinus Moravus. Additions by Johannes Basilius Augustonus}. (1495) Simon Bevilacqua, Venise. (en ligne: \url{https://www.digitale-sammlungen.de/en/view/bsb00048395}).\newline Munich, Bayerische Staatsbibliothek.}
\subsection{Littérature secondaire}
\label{sec:org3a944b4}
\hypertarget{citeproc_bib_item_6}{[6] José Chabás, « Astronomy for the Court in the Early Sixteenth Century: Alfonso de Córdoba and his Tabule Astronomice Elisabeth Regine ». in. \textit{Archive for history of exact sciences},. vol. 58,. nᵒ 3. p. 183‑217. 2004. (\url{http://dx.doi.org/10.1007/s00407-003-0073-2}).}

\hypertarget{citeproc_bib_item_7}{[7] José Chabàs \& Bernard Goldsein, \textit{A Survey of European Astronomical Tables in the Late Middle Ages}. (2012) Brill, Leyde (Pays-Bas), Boston (Massachussetts, États-Unis). Collection « Time, astronomy, and calendars: Texts and studies ». ISBN 978 90 04 23058 3.}

\hypertarget{citeproc_bib_item_8}{[8] José Chabás \& Bernard Goldstein, \textit{The Astronomical Tables of Giovanni Bianchini}. (2009) Brill, Leyde (Pays-Bas), Boston (Massachussetts, États-Unis). Collection « History of science and medecine library ». ISBN 978-90-04-17615-7.}

\hypertarget{citeproc_bib_item_9}{[9] Bacchelli di Franco, « GAURICO, Luca » in \textit{Dizionario Biografico degli Italiani}. (1999).}
\end{document}
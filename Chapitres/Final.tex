% Created 2023-05-28 dim. 23:23
% Intended LaTeX compiler: pdflatex
\documentclass[12pt]{article}
\usepackage[LAE, LGR, T2A, T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage[french]{babel}
\usepackage{setspace}
\usepackage{imakeidx}
\usepackage{textalpha}
\usepackage{arabtex}
\usepackage{utf8}
\usepackage[newfloat]{minted}
\usepackage[hidelinks]{hyperref}
\author{Clément Cartier\thanks{contact@catgolin.eu}}
\date{\today}
\title{Francisco García Ventanas et le patrimoine Espagnol (1640-1641)}
\hypersetup{
 pdfauthor={Clément Cartier},
 pdftitle={Francisco García Ventanas et le patrimoine Espagnol (1640-1641)},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.6-pre)}, 
 pdflang={French}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\maketitle
\tableofcontents
\newpage

\section{Introduction}
\label{sec:orgad7a2d8}
\subsection{Des alternatives aux tables alphonsines}
\label{sec:org263cfe3}
Après la publication du \emph{De revolutionibus} de Nicolas Copernic en 1543,
les éditions de Chrétien Wechel en 1545 et 1553, qui reprennent à l'identique les tables de Luca Gaurico
(voir chapitre \ref{chap:wechel}),
sont les seules éditions des tables alphonsines pendant près d'un siècle.

Entre temps, les \emph{Tabulae resolutae}, basées sur un modèle simplifié
de l'astronomie alphonsine, connaissent de nombreuses rééditions,
notamment grâce aux travaux de Johannes Schöner, partisan des thèses de Copernic.
Les \emph{Tabulae resolutae} sont éditées à Nuremberg en 1536 et en 1542
par Johann Petreius,
puis en 1551 et en 1561, toujours à Nuremberg, par Johann Von Berg,
Ulricus Neuber et Joannes Ferrarius,
et enfin par Mattheus Welack à Wittenberg en 1587 et 1588.

En 1551, Erasme Reinhold publie les \emph{Prutenicae tabulae coelestium motuum}
sous les presses de Ulrich Morhard, à Tübingen.
Ces dernières sont compilées à partir des théories de Copernic,
et donnent des résultats qui s'écartent du modèle alphonsin.
Emmanuel Poulle montre qu'à partir de 1564, les éphémérides ne sont plus calculés
avec les tables alphonsines ni avec les \emph{Tabulae resolutae}, mais avec
les tables rudolphines [\citeprocitem{4}{4}].
Ainsi, au début du \textsc{xvii}e siècle,
l'astronomie alphonsine semble bien morte,
remplacée par le nouveau modèle Copernicien.

Pourtant, en 1641,
plus d'un siècle après la dernière édition des tables alphonsines,
l'\emph{Officina Regia} de Madrid publie les
\emph{Tabulae Alphonsinae perpetuae motuum coelestium}.

\subsection{Contexte politique}
\label{sec:org5711b2b}
Après l'abdication de Charles Quint des royaumes d'Espagne en 1556,
les domaines de la maison Habsbourg ont été séparés en deux branches,
celle d'Espagne et celle d'Autriche (voir figure \ref{fig:orgf6a60b7}).
Le roi Philippe, arrière petit-fils de Charles Quint,
est l'héritier de la branche d'Espagne.
Lorsqu'il accède au trône en 1598, il règne alors sur plusieurs royaumes
qu'il cherche à unifier sous une seule couronne d'Espagne.
Parmi ces royaumes, on compte la Castille (avec les îles Canaries),
l'Aragon (parmi lequel on trouve la Catalogne, la Navarre, le royaume de Naples),
Grenade, la Sardaigne, la Sicile, le duché de Milan, les Pays Bas, la Franche Comté,
mais aussi les Indes Espagnoles en Amérique et aux Philippines,
et le Portugal avec toutes ses colonies, héritées par son père en 1580.
Cette grande alliance personnelle s'appelle l'Union Ibérique
(voir figure \ref{fig:orgc85e7bf}).
L'une des politiques principales politiques qui est mise en œuvre
pour tenter d'en garantir l'unité
consiste à imposer la culture Castillane et la religion catholique
sur l'ensemble du territoire.
Cette politique est notamment menée par Gaspar de Guzmán y Pimentel Ribera y Velasco de Tovar, comte d'Olivares (1587-1645)

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{/home/catgolin/.cache/org-persist/63/4d819f-74b4-472e-88d6-d15af4a54624-cd2807c16a40d2b4e7311e92b4fea898.png}
\caption{\label{fig:orgf6a60b7}Carte des domaines Européens de la maison de Habsbourg jusqu'en 1700: en rouge, les Habsbourg d'Espagne dont est issu Philippe IV et en jaune les Habsbourg d'Autriche, dirigé par Ferdinand Ernest de Habsbourg (1608-1657), cousin issu de germain de Felipe IV. (source: \url{https://commons.wikimedia.org/wiki/File:Habsbourg-1700.png?lang=fr\&userlang=fr})}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{/home/catgolin/.cache/org-persist/e9/010271-b231-48c0-b30d-1a6e17e8568f-29db133f0cccc994945fbea5a6dee16c.png}
\caption{\label{fig:orgc85e7bf}Carte des territoires de l'Union Ibérique en 1598: en rouge, les territoires régiés par le conseil des Indes, en vert par le conseil de Castille, en jaune par le conseil d'Aragon, en bleu par le conseil de Portugal, en marron par le conseil d'Italie et en violet par le Conseil de Flandre (source: \url{https://commons.wikimedia.org/wiki/File:Philip\_II\%27s\_realms\_in\_1598.png?uselang=fr})}
\end{figure}

En 1635, les conflits qui oppposaient les princes Protestants du Saint-Empire
aux soutiens de la maison de Habsbourg (les \og Impériaux\fg{})
dégénère dans un nouveau conflit entre la France et les Habsbourg,
notamment dirigé contre le roi Philippe.
Les hostilités mobilisent les armées espagnoles sur de très nombreux fronts,
ce qui complique le projet d'unification des Espagnes.
En 1640, ces complications se traduisent par plusieurs conflits internes,
d'abord en Catalogne avec des révoltes populaires, puis au Portugal
suite au soulèvement de plusieurs nobles.

\section{Bernardino Fernández de Velasco y Tovar et les tables de Francisco García Ventanas}
\label{sec:orgd5c0837}
\subsection{Conétable de Castille}
\label{sec:orgf86e280}
Bernardino Fernández de Velasco y Tovar (1609-1652)
est l'héritier d'une des principales familles de la noblesse castillane.
À la mort de son père en 1613, il hérite du titre de duc de Frías
et de conétable de Castille, et entre rapidement à la cour
du roi.
Il est par ailleurs le cousin germain du comte d'Olivares,
ce qui lui donne une position particulièrement privilégiée
[\citeprocitem{3}{3}].

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../images/illustrations/genealogie_velasco.png}
\caption{Généalogie de Bernardino Fernández de Velasco y Tovar et du comte d'Olivares}
\end{figure}

C'est à lui qu'entre 1640 et 1641, Francisco García Ventanas,
personnage inconnu dans l'histoire de l'astronomie en Espagne,
dédie la dernière édition des tables alphonsines.

Il est peut-être utile de noter également
avant de regarder le contenu de ces tables
que le comte d'Olivares semble avoir nourri un grand intérêt
pour l'astrologie. Cependant, je n'ai pas réussi
à retrouver la source de cette affirmation.

\subsection{Cahier ¶ du titre de 1641}
\label{sec:orgd21dc5a}
L'\emph{Universal Short Title Catalogue} recense 29 exemplaires
de l'édition des tables alphonsines de 1641,
dont 23 sont préservées en Espagne,
ce qui indique que ce titre n'a probablement pas connu
une très large diffusion.
Le témoin que j'ai pris pour référence principale
est préservé à la \emph{Biblioteca del Museo Naval} de Madrid
[\citeprocitem{1}{1}], et présente de nombreuses notes cursives
sur plusieurs pages annexées au volume.

L'édition comporte 32 cahiers de quatre folios
avec une signature en bas de la page sur les folios
avant la couture,
et un cahier initial de 8 folios (cahier ¶)
qui comporte la page de titre (¶1r),
une page d'errata (¶2v),
plusieurs pages de lettres d'autorisation
(¶2r et ¶3-4),
suivi d'une dédicace en castillan que Francisco García Ventanas
adresse à Bernardino Fernández de Velasco y Tovar,
reproduite et transcrite en annexe,
et une préface rédigée en latin.

Sur la page de titre, on peut lire:
\begin{quote}
\begin{paracol}{2}
Tabulæ Alphonsinae Perpetuæ motuum coelestium denuo restituæ
et illustratæ à Francisco Garcia Ventanas Mathematico.
\switchcolumn
Tables alphonsines des mouvements perpétuels du ciel
restaurées et illustrées par le mathématicien Francisco Garcia Ventanas
\switchcolumn*
Traduntur praecepta ut arithmeticæ colligantur
omnes medii motus, nec non festa mobilia secundum correctionem
Gregorianam, et tabulæ abbreviatæ eliciendi ei idem medios motus
constructæ ad Meridianum Toletanum,
cuius longitudo est 11 gradus.
\switchcolumn
Des instructions pour obtenir arithmétiquement
tous les mouvements moyens, ainsi que les fêtes mobiles
selon la correction grégorienne sont donnés,
ainsi que des tableaux abrégés pour obtenir les mêmes mouvements moyens
construits pour le méridien de Tolède,
dont la longitude est 11 degrés.
\switchcolumn*
Cum privilegio
\switchcolumn
Avec privilèges
\switchcolumn*
Matriti in Officina Regia, Anno 1641\\
[\citeprocitem{1}{1} f.¶1r]
\switchcolumn
Madride, Atelier Royal, Année 1641.
\end{paracol}
\end{quote}

Les lettres d'approbation insistent sur le fait
que cette édition restitue la gloire passée des tables espagnoles,
et corrige les \og erreurs\fg{} de leur temps.
Ainsi, le clerc Andreas de León écrit en latin:
\begin{quote}
\begin{paracol}{2}
Franciscus Garcia [\ldots{}] Hispanam gloriam fæculis restituit, Calculos certiores, quando plerumque cæteri per statum seriem deviant, memoriæ redegit
[\citeprocitem{1}{1} f.¶3v]
\switchcolumn
Francisco Garcia [\ldots{}] a restauré la gloire ancienne de l'Espagne
et à rendu les calculs plus fiables, quand beaucoup d'autres dérivaient
par une série d'étapes, et les a fait revenir à la mémoire.
\end{paracol}
\end{quote}
Plus loin, un certain Josephus Martinez ajoute,
également en latin, que les tables sont à la fois conformes
à l'héritage ancien, et comportenent en même temps une part de nouveauté\footnote{\og quidquid antiquitate firmum, quidquid novitate præclarum, mira rationum concatenatione, et connexum et digestum\fg{}
[\citeprocitem{1}{1} f.¶4v]\\
\og tout ce qui est d'une forte ancienneté,
tout ce qui se distingue par la nouveauté,
ce qu'il y a de prodigieux dans l'enchaînement de la raison,
et dans le corolaire et dans la classification\fg{}}

Enfin, on trouve la dédicace de l'auteur
au Duc de Feria.
Dans cette dédicace, on apprend que les travaux de Peurbach,
Regiomontanus, Copernic, Reinhold, Tycho Brahe, Kepler et Philippe de Lansberg
ont dépassé et remplacé les tables alphonsines.
Cependant, il prétend montrer que ces dernières comprenaient déjà
toute la vérité de l'astronomie, que leurs travaux dérivent seulement
de ceux du Roi Alphonse, et que leur refus d'accorder le crédit
qui est dû au Roi Alphonse serait lié à la tentation de sapper les intérêts
Espagnols.
Le Conétable de Castille semble avoir été intéressé un moment par ce projet,
mais avoir changé d'avis au cours de la rédaction.
L'auteur promet donc, s'il est payé pour son travail,
de faire un traité sur l'astrologie (dont je n'ai pas trouvé trace),
avant de se consacrer à des travaux sur la navigation.
Les personnes en charge de la conservation de l'imprimé
à la \emph{Biblioteca del Museo Naval} semblent d'ailleurs penser
que ces tables ont pu avoir un intérêt pour la Marine Espagnole
[\citeprocitem{2}{2}], mais je n'ai pas trouvé beaucoup d'éléments qui attestent
cette affirmation, à part le catalogue de cette bibliothèque.

La 

\section{Contenu de l'édition de 1641}
\label{sec:org96344ab}
Le contenu de l'ouvrage commence sur la première page du cahier A,
folioté avec le numéro 1 imprimé en haut de la page.
Jusqu'à la fin du recto du folio V2, il contient des canons
qui semblent avoir été rédigés par Francisco García Ventanas.
Le recto donne des tables pour calculer tous les moyens moyvements,
avec une seule ligne pour chaque mouvement.
Si on considère le moyen mouvement comme une équation linéaire
de la forme \(m(t)=at+b\), ces tables donnent en fait le coefficient directeur
du graphe de la fonction, c'est-à-dire le coefficien \(a\).
Aucun autre imprimé ne propose de telles tables.
Les tables de moyens mouvements qui correspondent aux tables habituelles,
c'est-à-dire avec les valeurs du moyen mouvement \(m\) à différentes dates,
sont reléguées à la fin de l'ouvrage.

Le folio V3 et le recto du folio V4 donnent la table de l'équation du Soleil,
puis on a la table de l'équation du mouvement d'accès et recès de la huitième sphère
au verso.
On trouve ensuite les tables d'ascension droite sur deux folios
qui semblent avoir été annexés (folios x1 et x2),
puis les tables d'équation de la Lune (folios X1 à X3),
la table de la latitude de la Lune (recto du folio X4).

Viennent ensuite toutes les tables d'équation des planètes,
depuis le verso du folio X4 jusqu'à la fin du recto du cahier Bb3.
À la suite des tables d'équation, il y a les tables de passions
(folios Bb3v-Cc1v),
puis les tables dédiées au calcul d'éclipses (folios Cc2 jusqu'au verso du folio Dd4).

La dernière page du cahier Dd et la première page du cahier Ee
sont dédiées à des tables pour le calcul
du temps écoulé depuis le début de l'ère du Christ,
puis l'ensemble des cahiers Ee et Hh sont dédiés à la carte des étoiles.

À partir de la dernière page du cahier Hh,
l'éditeur a ajouté des tables sous le titre
\emph{Tabulae Breves Ad Elisciendos medios Motus Planetarum secundum calculum Regis Alphonsi}.
La première est une table qui permet de convertir une date
exprimée dans le calendrier Grégorien en un temps exprimé dans le système
d'unités sexagésimales de l'astronomie alphonsine.
Le cahier Ii donne ensuite toutes les tables pour le moyen mouvement des planètes,
avec des arguments de 1 à 10, puis seulement les dizaines de 10 à 60.

Enfin, sur la dernière page,
on peut lire \emph{En Madrid, En la Imprenta Real An̄o 1640}.

\section{Conclusion}
\label{sec:org8b6bca6}
Il semble que l'édition des tables alphonsines en 1640-1641
à la cour du roi des Espagnes n'a pas été motivé
par un usage en astronomie, astrologie ou mathématiques.
Dans la dédicace, l'auteur invite lui-même son patron
à garder l'ouvrage dans un coin de sa bibliothèque,
même s'il ne semble pas avoir l'intention de l'utiliser.
L'enjeu est moins de faire de l'astronomie
que de montrer que l'Espagne est capable de produire
des travaux en astronomie.
En rappelant l'importance des tables alphonsines
dans les développements de la discipline au cours des siècles précédents,
et surtout en rappelant l'origine Castillane de ces tables,
l'ouvrage semble principalement servir à commémorer
les accomplissements de \og l'Espagne\fg{} dans les sciences.

Aussi, je crois que la principale motivation de cette dernière édition
des tables alphonsines est de les revendiquer comme patrimoine
qui puisse être valorisés comme argument de l'unité et de la puissance
des souverains castillans au sein de l'Union Ibérique.
Tous les titres étudiés dans ce mémoire montrent les signes
que cet enjeu de patrimonialisation des tables alphonsines,
notamment à des fins politiques,
s'est toujours manifesté sous différentes formes,
et cette édition du \textsc{xvii}e siècle, produite dans un contexte
éloigné de celui de l'Italie du \textsc{xvi}e siècle, ne fait pas exception.
\section{Bibliographie}
\label{sec:orgdd441bf}
\subsection{Sources primaires}
\label{sec:orgcd0a784}
\hypertarget{citeproc_bib_item_1}{[1] CF-184, \textit{Tabulae Alphonsinae perpetuae motuum coelestium} Madrid : Officina Regia, 1641. (\url{https://catedranaval.files.wordpress.com/2021/10/tablas-alfonsies.pdf}). (Madrid, Biblioteca del Museo Naval).}
\subsection{Littérature secondaire}
\label{sec:org4edc257}
\hypertarget{citeproc_bib_item_2}{[2] Carmen Torres López, \href{https://catedranaval.com/2021/10/27/tabulae-alphonsinae-perpetuae-motuum-coelestium-denuo-restitutae-illustratae-1641/}{« Las Tabulas Alfonsíes: Tabulae Alphonsinae perpetuae motuum coelestium denuo restitutae \& illustratae. Matriti: Oficina Regia, 1641. Tiré de la présentation «toledo y la obra astronómica de alfonso x el sabio» »}. Présenté à . , El hombre y el cosmos, Año International de la Astronomía. 2009.}

\hypertarget{citeproc_bib_item_3}{[3] Santiago Martínez Hernández, \href{https://dbe.rah.es/biografias/38318/bernardino-fernandez-de-velasco-y-tovar}{« Bernardino Fernández de Velasco y Tovar »}. in. Real Academia de la Historia (éd.) . \textit{Diccionario Biográfico electrónico}.}

\hypertarget{citeproc_bib_item_4}{[4] Emmanuel Poulle \& Denis Savoie, « La survie de l’astronomie alphonsine ». in. \textit{Journal for the history of astronomy},. vol. 29,. p. 201‑207. 1998.}
\end{document}
% Created 2023-05-22 lun. 13:25
% Intended LaTeX compiler: pdflatex
\documentclass[12pt]{article}
\usepackage[LAE, LGR, T2A, T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage[french]{babel}
\usepackage{setspace}
\usepackage{imakeidx}
\usepackage{textalpha}
\usepackage{arabtex}
\usepackage{utf8}
\usepackage[newfloat]{minted}
\usepackage[hidelinks]{hyperref}
\author{Clément Cartier\thanks{contact@catgolin.eu}}
\date{\today}
\title{}
\hypersetup{
 pdfauthor={Clément Cartier},
 pdftitle={},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.6-pre)}, 
 pdflang={French}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\tableofcontents
\newpage
\section{Architecture générale de l'application}
\label{sec:org9c65909}
Pour étudier le contenu des éditions imprimées des tables alphonsines,
j'ai construit un logiciel\footnote{Le code source de l'application
et une documentation plus précise de son fonctionnement
peut être trouvée sur Framagit,
et est éventuellement ouvert aux contributions:
\url{https://framagit.org/Catgolin/pat-explorer/}},
dont l'esprit consiste à mettre en relation deux applications.
\subsection{Modélisation du «cadre historique»}
\label{sec:orgb3179cb}
La première (\mintinline{python}{scenery}) décrit le «cadre» dans lequel sont produites les éditions,
en collectant notamment des informations sur les personnes et sur les lieux
qui participent à l'histoire que j'étudie.
Pour identifier les personnes physiques mentionnées dans les textes,
je me suis référé aux bases de données d'autorité internationales:
je cherche ainsi les personnes qu'au moins une autorité
associe aux noms que je rencontre dans les éditions,
en vérifiant si les autres informations biographiques associées à cette personne
dans les bases de données concordent avec les informations données par l'édition\footnote{Voir les méthodes \mintinline{python}{def get_from_names} et
\mintinline{python}{def get_from_name} du \mintinline{python}{class Manager}
de l'entité \mintinline{python}{class Actor}:
\url{https://framagit.org/Catgolin/pat-explorer/-/blob/main/scenery/managers.py\#L44}}.
En corrigeant manuellement certaines erreurs qui ont pu être produites
par ce programme d'identification, j'ai pu ainsi identifier chaque acteur
et chaque actrice par un nom unique avec une orthographe standard
qui correspond à la manière la plus courante de les désigner
dans les bases de données bibliographiques francophones.
Pour les lieux, j'ai exploité la base de donnée \href{https://geonames.org}{geonames.org},
principalement pour faciliter l'alignement avec les informations
fournies par les bases de données du \emph{Consortium of European Research Libraries}.
\subsection{Modélisation des «tables»}
\label{sec:org67f79fc}
La seconde (\mintinline{python}{astronomical_content})
décrit le fonctionnement mathématique des tables
et leur interprétation astronomique.
Par la notion de «table»,
je désigne un objet qui peut être modélisé sous la forme d'une fonction:
les valeurs qu'on utilise pour naviguer dans la table sont les arguments
de cette fonction, et les valeurs
qui correspondent à l'argument recherché dans la table
sont les paramètres de sortie de cette fonction, qu'on appelle également
les paramètres explicites de la table.
La bibliothèque \mintinline{python}{Kanon}\footnote{J'ai utilisé
la version 0.6.5 de la bibliothèque. Voir la documentation:
\url{https://kanon.readthedocs.io/}}
propose ainsi une modélisation des tables
sous la forme de \mintinline{python}{<class 'function'>}
en Python,
à partir notamment de données extraite de l'édition des tables alphonsines
dans le cadre du projet \textsc{Dishas}.
Les tables ainsi modélisées par la bibliothèque \mintinline{python}{Kanon}
exécutent notamment des opérations arithmétiques (additions, soustractions,
calcul de fonctions trigonométriques, etc.) pour déterminer la valeur
des paramètres explicites de la table correspondant aux arguments donnés.
Par exemple, les tables du moyen mouvement peuvent être décrites
par des fonctions de la forme \(x\mapsto ax+b\),
où \(b\) est la racine du mouvement et \(a\) est la différence
entre deux entrées de la table séparées d'une unité.
Aussi, toutes les tables de moyen mouvement peuvent être regroupées
dans une même famille de fonctions qui dépendent de deux paramètres,
\(a\) et \(b\), qu'on appelle des paramètres implicites de la table.
Pour obtenir une table du moyen mouvement, il faut donc injecter
la valeur de ces deux paramètres dans la fonction générique du moyen mouvement.
Souvent, les paramètres implicites dépendent notamment de l'objet astronomique
qui est étudié.

Partant de ces constats, mon approche a consisté à copier
la modélisation proposée dans le cadre du projet \textsc{Dishas},
en considérant un «type de table»
(\mintinline{python}{class TableType})
comme une fonction de la bibliothèque \mintinline{python}{Kanon},
éventuellement associée à un objet astronomique
(\mintinline{python}{class AstronomicalObject}).
Les «tables» contenues dans les imprimés correspondent donc
à un «type de table», qui me sert d'exemple de référence
pour comparer les arguments et les paramètres de plusieurs tables du même type.

Par ailleurs, José Chabás a proposé dans \emph{Computational Astronomy in the Middle Ages}
[\citeprocitem{1}{1}] une description des tables appartenant
à différents ensembles.
Son approche a consisté à identifier comme «table»\footnote{En anglais, \emph{table}
aurait aussi pu être traduit comme «tableau» mais le terme \emph{tabular} me semblerait
alors plus adéquat.}
des objets qui correspondent à plusieurs «tables»
selon la définition adoptée plus haut,
regroupées ensemble sur la page.
Ces «tables» identifiées par José Chabás, je les appelle des «tableaux».
Pour chaque type de table, j'ai renseigné les types de tableaux que José Chabás
identifie comme faisant partie du corpus Alphonsin et qui contiennent
ce type de table, pour assurer l'alignement de mon modèle
avec les descriptions en usage dans la littérature secondaire.

\section{Modélisation des sources}
\label{sec:org2bd56bf}
\subsection{Définition des corpus}
\label{sec:orgcd8af8a}
Dans les canons qui accompagnent les tables,
on trouve la description d'un ensemble de procédures.
Comme on le voit dans les chapitres \ref{chap:ratdolt} et \ref{chap:santritter},
ces procédures sont imbriquées dans un réseau d'inclusions et de dépendances,
de sorte que la réunion des tables et des procédures forme un ensemble cohérent,
que j'appelle le logiciel alphonsin.
Chaque canon et chaque nouvelle édition des tables donne une version légèrement
différente de ce logiciel.
Pour autant, l'unité du logiciel reste implicitement identifiable
au travers de ses différentes versions.

Dans le cadre proposé
par l'\emph{International Federation of Library Associations and Institutions}
au sein de leur \emph{Fonctionnal Requirements for Bibliographic Records} [\citeprocitem{4}{4}],
une «œuvre» (\emph{work}) est une création intellectuelle originale et significative
qui peut être attribuée à un auteur ou une autrice,
et qui prend la forme d'une ou plusieurs «expressions» (\emph{expression}):
l'expression peut être la traduction de l'œuvre dans une langue écrite,
sa performance orale, etc.
Les études menées sur le logiciel alphonsin dans le cadre du projet
\textsc{Alfa} montrent que la création de ce «logiciel» ne peut pas être considéré
comme la création originale d'un auteur ou d'une autrice,
mais qu'elle est le résultat d'un long processus dans lequel plusieurs œuvres
ont été créées et dérivées par des acteurs et actrices différents.
Parmi les œuvres ainsi créées dans la constitution du logiciel alphonsin,
on trouve par exemple les canons de Jean de Saxe, qui accompagnent l'édition
\emph{princeps} des tables alphonsines (voir chapitre \ref{chap:ratdolt}).

Si on considère les versions majeures et mineures du «logiciel» alphonsin
selon les définitions proposées pour les versions sémantiques [\citeprocitem{3}{3}],
les «œuvres» alphonsines peuvent être vues comme autant de versions du «logiciel».
Par ailleurs, certaines œuvres ont proposé des «logiciels» qui sont des dérivations
du logiciel alphonsin, et/ou qui en dépendent.
C'est le cas par exemple des tables composées pour la «reine catholique»
(voir chapitre \ref{chap:liechtenstein}).

Dans le cadre de ce mémoire, j'ai fait le choix de ne pas étudier
les différentes «œuvres» présentes dans les éditions.
En effet, cela aurait nécessité d'étudier les canons,
et donc de produire une traduction pour toutes les éditions,
ce qui n'était pas envisageable dans le temps imparti.
Aussi, je n'ai pas inscrit ce niveau dans mon modèle de données.
Pour autant, il me fallait un moyen de distinguer entre les œuvres
«alphonsines» et celles d'autres «logiciels»,
ce que j'ai fait en introduisant la notion de \mintinline{python}{class Corpus}
pour délimiter, dans la liste des sources identifiées par les chercheurs
et chercheuses du projet \textsc{Alfa},
celles qui étaient directement d'intérêt pour mon étude
et celles qui permettaient d'éclairer le contexte plus général
de l'évolution du logiciel alphonsin,
selon des critères définis plus bas.

\subsection{«Titres» et «témoins»}
\label{sec:org1c9ba63}
Le cadre des \emph{Fonctional Requirements for Bibliographic Records}
[\citeprocitem{4}{4}] suppose que les expressions d'une œuvre
sont incarnées par des \emph{manifestations},
qui correspond notamment à l'édition de cette expression
à une date précise, et que je traduis parfois ici avec le terme
d'«ouvrage».
Une \emph{manifestation} est par ailleurs exemplifiée dans des \emph{items},
qui correspondent dans le contexte de ce mémoire
aux exemplaires physiques issus de l'édition,
et qui sont conservés notamment dans des bibliothèques.
Cette distinction entre \emph{manifestation} et \emph{item} correspond,
dans le cas des livres imprimés, à celle que propose Joseph Dane
entre \emph{book} et \emph{book-copy}\footnote{Voir [\citeprocitem{2}{2}]}.

Pour traduire la notion de \emph{manifestation},
je me suis inspiré du modèle proposé par le Consortium of European Research Libraries,
qui entretient la base de donnée de l'\emph{Incunabala Short Title Catalogue}.
Cette base de donnée recense l'ensemble des incunables connus (des \emph{items}),
et les rassemble sous des «Titres»,
qui correspondent à la \emph{manifestation} dont l'incunable considéré est un exemple.

En identifiant ainsi les \emph{manifestations} au \mintinline{python}{class Title},
je me suis également donné un moyen d'identifier facilement chaque ouvrage:
les livres imprimés n'ont pas toujours un «titre» clairement identifié
sur la première page.
Le titre attribué par l'\emph{Incunabala Short Title Catalogue}
a un caractère arbitraire, mais il permet d'identifier l'ouvrage
par un nom unique, et le caractère international de la base de donnée
fait de ce nom une référence largement partagée.
Pour les post-incunables, j'ai utilisé d'autres bases de données disponibles
pour identifier le titre de l'ouvrage.

En identifiant certaines chaînes de caractères qui sont généralement incluses
dans les titres de différents types d'ouvrages, j'ai également pu utiliser
ces titres pour délimiter les corpus,
dont j'ai dû ensuite affiner manuellement le contenu.

Enfin, mes sources primaires correspondent à ce que les \emph{Fonctional Requirements for Bibliographic Records}
décrivent comme les \emph{items}.
Dans le cadre de la réflexion sur le modèle de données
du projet \textsc{Eida},
il s'est cependant avéré que cette notion d'\emph{item}
n'était pas suffisamment adaptée pour étudier des manuscrits
ou des imprimés anciens.
En effet, d'une part il arrive souvent que plusieurs ouvrages
soient reliés ensemble dans un même volume.
C'est le cas de l'exemplaire C de l'édition de 1483
préservé à la Bayerische Staatsbibliotek (voir \ref{witnesses:ratdolt}).
C'est également le cas des éditions du \textsc{xvi}e siècle
dont beaucoup ont été reliées avec les tables de la «reine catholique»
(voir \ref{chap:liechtenstein}).
Une autre situation, qui ne se présente pas dans mon corpus,
est celle d'un ouvrage édité sous la forme de plusieurs volumes.
Aussi, j'ai modélisé les \emph{items} sous la forme de «témoins»
(\mintinline{python}{class Witness}),
qui correspondent à la partie d'un exemplaire physique
contenant un volume de l'ouvrage.

\section{Analyse du contenu des imprimés}
\label{sec:orgb3e2700}
Une fois les sources des différentes éditions identifiées,
j'ai pu parcourir les témoins de chaque édition
pour repérer les tables contenues dans chaque témoin,
en leur attribuant un type.

Un volet absent de ce modèle qu'il aurait été intéressant d'intégrer
(mais qui aurait nécessité trop de temps dans le cadre de ce mémoire)
aurait été de tenir compte de la disposition des tableaux sur la page.
En particulier, lorsque plusieurs tables sont regroupées dans un tableau,
il aurait été intéressant de regarder la manière dont les différents arguments
et les différents paramètres sont agencés.
La construction des tableaux peut parfois exploiter des propriétés de la table
qui ne sont pas faciles à identifier au travers de la modélisation informatique
des tables.
La disposition des tableaux répond également à des contraintes,
notamment spatiales, dont la modélisation des tables ne rend pas compte non plus.

\hypertarget{citeproc_bib_item_1}{[1] José Chabás, \textit{Computational Astronomy in the Middle Ages: Sets of Astronomical Tables in Latin} Madrid (Espagne) : Consejo superior de investigaciones cieníficas, 2019. ISBN : 978-84-00-10558-7.}

\hypertarget{citeproc_bib_item_2}{[2] Joseph Dane, \textit{The Myth of Print Culture: Essays on Evidence, Textuality, and Bibliographical Method} Toronto (Ontario, Canada) : University of Toronto Press, 2003.}

\hypertarget{citeproc_bib_item_3}{[3] Tom Preston-Werner, Phil Haack, Isaacs, et al., \textit{Semantic Versioning specification} édition 2, 2013. (\url{https://github.com/semver/semver}). (Publication originale (v1.0.0) le 7 septembre 2011).}

\hypertarget{citeproc_bib_item_4}{[4] IFLA Study Group on the functional Requirements for Bibliographic Records, \textit{Fonctional Requirements for Bibliographic Records}.(vol.19) International Federation of Library Associations and Institutions, 2009. ISBN : 978359811382-6. (\url{https://repository.ifla.org/handle/123456789/811}).}
\end{document}
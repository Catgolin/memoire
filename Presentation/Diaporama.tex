% Created 2023-02-21 mar. 19:51
% Intended LaTeX compiler: pdflatex
\documentclass[presentation]{beamer}
\usepackage[LAE, LGR, T2A, T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage[hidelinks]{hyperref}
\usepackage[french]{babel}
\usepackage{setspace}
\usepackage{textalpha}
\usepackage{arabtex}
\usepackage{utf8}
\usepackage{svg}
\institute{M2 Histoire et philosophie des sciences\\ Université Paris Cité\\ Sous la direction de Matthieu Husson (Syrte, Observatoire de Paris, CNRS) \and Sara Confalonieri (Sphere, Université Paris Cité, CNRS)}
\mode<beamer>{\usetheme{Antibes}}
\mode<beamer>{\usecolortheme{beaver}}
\AtBeginSection[]{\begin{frame}<beamer>\frametitle{}\tableofcontents[currentsection]\end{frame}}
\usetheme{default}
\author{Clément Cartier <contact@catgolin.eu>}
\date{\textit{<2023-02-24 ven. 14:00>}}
\title{Astronomie médiévale sous les presses humanistes}
\subtitle{Patrimonialisation des tables alphonsines dans la culture des imprimés (1483--1641)}
\hypersetup{
 pdfauthor={Clément Cartier <contact@catgolin.eu>},
 pdftitle={Astronomie médiévale sous les presses humanistes},
 pdfkeywords={Histoire de l'astronomie, Histoire des mathématiques, Moyen Âge, Histoire du livre,},
 pdfsubject={Présentation de mémoire à mi-parcours},
 pdfcreator={Emacs 27.1 (Org mode 9.6-pre)}, 
 pdflang={French}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\maketitle

\section{Contexte}
\label{sec:orge3090c9}
\begin{frame}[label={sec:org0795929}]{Les tables alphonsines}
\begin{columns}
\begin{column}{0.5\columnwidth}
\begin{block}{Histoire des tables alphonsines}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./transmission.jpg}
\caption{Circulation et évolution du corpus alphonsin à partir de (\citeprocitem{2}{Chabás 2019})}
\end{figure}
\end{block}
\end{column}

\begin{column}{0.5\columnwidth}
\begin{block}{Un corpus connecté}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{/home/catgolin/Documents/Notes/Lectures/Canons-Saxe-Poulle/deductif-fdp.png}
\caption{Graphe des références internes entre les canons de Jean de Saxe (\citeprocitem{11}{Poulle 1984}) réalisé en Python}
\end{figure}
\end{block}
\end{column}
\end{columns}
\end{frame}

\section{État de l'art}
\label{sec:orgd8b0ed6}
\begin{frame}[label={sec:orgd921d23}]{Histoire des tables}
\begin{columns}
\begin{column}{0.5\columnwidth}
\begin{block}{Travaux précurseurs}
\begin{itemize}
\item (\citeprocitem{12}{Poulle \& Savoie 1998})
\item[{HTN}] Dominique Tournes (2009-2013)
\item[{SAW.2}] Karine Chemla, Agathe Keller, Christine Proust (2013-2016)
\item[{ALFA}] Matthieu Husson (2016-2022)
\end{itemize}
\end{block}
\end{column}
\begin{column}{0.5\columnwidth}
\begin{block}{Outils d'humanités numériques}
\begin{itemize}
\item Base de donnée des tables
\begin{itemize}
\item Alphonsines
\item Ptolémaïques
\item Indiennes
\end{itemize}
\item Outils de modélisation et d'édition de tables numériques
\item Outils d'analyse des paramètres de calcul des tables
\end{itemize}
\end{block}
\end{column}
\end{columns}
\end{frame}
\begin{frame}[label={sec:org81695ba}]{Histoire des imprimés}
\begin{columns}
\begin{column}{0.5\columnwidth}
\begin{block}{Historiographie en plein développement}
\begin{itemize}
\item Elizabeth Eisenstein, Antony Grafton (\citeprocitem{3}{Eisenstein Grafton \& Johns 2002}, \citeprocitem{5}{Grafton 1980}, \citeprocitem{4}{2020})
\item (\citeprocitem{10}{Pantin 2010}, \citeprocitem{8}{2012a}, \citeprocitem{9}{2012b}, \citeprocitem{7}{2020})
\begin{itemize}
\item 1474--1490: premières tentatives
\item 1495--1515: stabilisation
\item 1525--1535: renouveau
\item Après 1535: Copernic
\end{itemize}
\end{itemize}
\end{block}
\end{column}
\begin{column}{0.5\columnwidth}
\begin{block}{Développement des calendriers}
\begin{itemize}
\item Richard Kremer : Almanach, Éphémérides, Kalendarium
\end{itemize}
(\citeprocitem{1}{Brévart 1988}, \citeprocitem{6}{Lüsebrink 2004})
\end{block}
\end{column}
\end{columns}
\end{frame}
\section{Problématique}
\label{sec:org0e1d8b0}
\begin{frame}[label={sec:org50c0657}]{De l'impression à la patrimonialisation}
\begin{definition}[Problématique]
En quoi la traduction des tables alphonsines dans la culture des imprimés contribue-t-elle à la patrimonialisation d'un instrument pratique de l'astronomie médiévale ?
\end{definition}
\end{frame}
\begin{frame}[label={sec:org6ccf517}]{L'image d'une tradition alphonsine}
\begin{columns}
\begin{column}{0.5\columnwidth}
\begin{block}{Histoire des tables}
\begin{description}
\item[{José Chabás}] Diffusion des canons et \emph{membra adjuncta} (\emph{tempus est mensura motus})
\item[{Richard Kremer}] Évolution des tables (équation du Soleil)
\item Calendriers et éphémérides
(\textsc{xv}e-\textsc{xvii}e siècle)
\end{description}
\end{block}
\end{column}
\begin{column}{0.5\columnwidth}
\begin{block}{Historiographie}
\begin{description}
\item[{Matthieu Husson et}] 

\item[{Jean-Claude Penin}] Historiographie de l'astronomie alphonsine, depuis le manuscrit 7281 (1445/1460) jusqu'à Emmanuel Poulle (c.1980)
\end{description}
\end{block}
\end{column}
\end{columns}
\end{frame}
\section{Sources}
\label{sec:org1aaf4c5}
\begin{frame}[label={sec:orgd25a079}]{Environ 40 imprimés, dont une dizaine «alphonsins»}
\begin{columns}
\begin{column}{0.5\columnwidth}
\begin{block}{Plusieurs catégories de sources}
\begin{center}
\includegraphics[width=.9\linewidth]{./repartition-corpus.png}
\end{center}

\begin{center}
\includegraphics[width=.9\linewidth]{./categories-sources.png}
\end{center}
\end{block}
\end{column}

\begin{column}{0.5\columnwidth}
\begin{block}{Plusieurs lieux de production}
\begin{center}
\includegraphics[width=.9\linewidth]{./lieux-sources.png}
\end{center}

\begin{center}
\includegraphics[width=.9\linewidth]{./chronologie-villes.png}
\end{center}
\end{block}
\end{column}
\end{columns}
\end{frame}
\section{Plan}
\label{sec:org20f9342}
\begin{frame}[label={sec:org7aac6f1}]{Plan de travail}
\begin{enumerate}
\item Identification des tables dans les imprimés
\item Rapprochement avec les tables manuscrites
\item Analyse des discours et des préfaces
\end{enumerate}
\end{frame}
\begin{frame}[label={sec:org128e233}]{Plan chronologique}
\begin{columns}
\begin{column}{0.5\columnwidth}
\begin{block}{Plan}
\begin{enumerate}
\item 1480--1490 : développement de l'imprimerie
\item 1490--1520 : montée en puissance de l'astronomie imprimée
\item 1520--1543 : déclin et remplacement
\item 1543--1641 : patrimonialisation
\end{enumerate}
\end{block}
\end{column}
\begin{column}{0.5\columnwidth}
\begin{block}{Chronologie du corpus}
\begin{center}
\includegraphics[width=.9\linewidth]{./alphonse-bpr.png}
\end{center}
\end{block}
\end{column}
\end{columns}
\end{frame}
\section{Bibliographie}
\label{sec:org41c5135}
\begin{tiny}
\hypertarget{citeproc_bib_item_1}{[1] Francis Brévart, “The German Volkskalender of the Fifteenth Century.” in. \textit{Speculum},. vol. 63,. pp. 312–342. 1988.}

\hypertarget{citeproc_bib_item_2}{[2] José Chabás, \textit{Computational Astronomy in the Middle Ages, Sets of Astronomical Tables in Latin} Madrid, Espagne : Consejo Superior de Investigaciones Científicas, 2019.}

\hypertarget{citeproc_bib_item_3}{[3] Elizabeth Eisenstein, Anthony Grafton, \& Adrian Johns, “How Revolutionary Was the Print Revolution?” in. \textit{American historical review},. vol. 107,. pp. 84–128. 2002.}

\hypertarget{citeproc_bib_item_4}{[4] Anthony Grafton, \textit{Inky Fingers: The Making of Books in Early Modern Europe} Cambridge, Massachusetts (États-Unis) : Bellknap Press of Harvard University Press, 2020.}

\hypertarget{citeproc_bib_item_5}{[5] Anthony Grafton, “The Importance of Being Printed.” in. \textit{Journal of interdisciplinary history},. vol. 11,. pp. 265–286. 1980.}

\hypertarget{citeproc_bib_item_6}{[6] Hans-Jürgen Lüsebrink, “La littérature des almanachs: réflexions sur l’anthropologie du fait littéraire.” in. \textit{Études françaises},. vol. 36,. no. 3. pp. 47–64. 2004. (\url{http://dx.doi.org/10.7202/009722ar}).}

\hypertarget{citeproc_bib_item_7}{[7] Isabelle Pantin, “Borrowers and Innovators in the History of Printing Sacrobosco: The Case of the in-Octavo Tradition.” in. Matteo Valleriani (ed.) . \textit{De Sphaera of Johannes de Sacrobosco in the Early Modern Period: The Authors of the Commentaries}. pp. 265–312. Cham, Zoug (Confédération Suisse) : Springer Cham,. 2020.}

\hypertarget{citeproc_bib_item_8}{[8] Isabelle Pantin, “The First Phases of the Theoricae Planetarum Printed Tradition (1474-1535): The Evolution of a Genre Observed through Its Images.” in. \textit{Journal for the history of astronomy},. vol. 43,. pp. 3–26. 2012a.}

\hypertarget{citeproc_bib_item_9}{[9] Isabelle Pantin, \href{https://books.openedition.org/enc/507}{“Le style typographique des ouvrages scientifiques publiés par Michel de Vascosan.}” in. \textit{Passeurs de textes. Imprimeurs et libraires à l’âge de l’humanisme}. pp. 167–184. Paris (France) : École nationale des chartes,. 2012b.}

\hypertarget{citeproc_bib_item_10}{[10] Isabelle Pantin, “The Astronomical Diagrams in Oronce Finé’s Protomathesis (1532): Founding a French Tradition?” in. \textit{Journal for the history of astronomy},. vol. 41,. pp. 287–310. 2010.}

\hypertarget{citeproc_bib_item_11}{[11] Emmanuel Poulle (ed.), \textit{Les tables alphonsines avec les canons de Jean de Saxe} Paris, France : Éditions du Centre national de la recherche scientifique, 1984.}

\hypertarget{citeproc_bib_item_12}{[12] Emmanuel Poulle \& Denis Savoie, “La survie de l’astronomie alphonsine.” in. \textit{Journal for the history of astronomy},. vol. 29,. pp. 201–207. 1998.}
\end{tiny}
\end{document}
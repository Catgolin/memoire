#+title: Astronomie médiévale des presses humanistes à l'âge classique
#+subtitle: Patrimonialisation des tables alphonsines dans la culture des imprimés (1483--1641)
#+author: Clément Cartier <contact@catgolin.eu>
#+email:
#+date: <2023-05-31 ven. 14:30+00:15>
#+description: Soutenance
#+keywords: Histoire de l'astronomie,
#+keywords: Histoire des mathématiques,
#+keywords: Moyen Âge,
#+keywords: Histoire du livre,

#+startup: beamer
#+latex_class: beamer
#+options: toc:nil
#+bibliography: ../bibliography.bib
#+bibliography: ~/Documents/Notes/Lectures/bibliography.bib
#+cite_export: csl ~/org/citation-diapo.csl

#+latex_header: \institute{M2 Histoire et philosophie des sciences\\ Université Paris Cité\\ Sous la direction de\\Matthieu Husson (Syrte, Observatoire de Paris, CNRS)\\et\\Sara Confalonieri (Sphere, Université Paris Cité, CNRS)}
#+latex_header: \mode<beamer>{\usetheme{Antibes}}
#+latex_header: \mode<beamer>{\usecolortheme{beaver}}
#+latex_header: \AtBeginSection[]{\begin{frame}<beamer>\frametitle{}\tableofcontents[currentsection]\end{frame}}

#+latex_header: \setbeamerfont{caption}{size=\tiny}
#+latex_header: \setbeamertemplate{footline}[frame number]

#+beamer_frame_title: 1
#+beamer_frame_level: 2

#+latex_header: \usepackage{tikz}
#+latex_header: \usetikzlibrary{mindmap}
#+latex_header: \usepackage{txfonts}

#+begin_export latex
% source: stackoverflow
\tikzset{
  invisible/.style={opacity=0, text opacity=0},
  visible on/.style={alt={#1{}{invisible}}},
  alt/.code args={<#1>#2#3}{%
    \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}} %
  },
}
#+end_export

* Introduction
*** Problem statement and thesis
***** Problem statement                                         :B_block:
:PROPERTIES:
:BEAMER_env: block
:END:
- How did the "Alfonsine Tables" evolve after they started to be printed
\pause
***** Thesis                                                    :B_block:
:PROPERTIES:
:BEAMER_env: block
:END:
- "Humanist" goals infused in the "print cultures"
  - Transmission of a heritage from the past
  - Exploiting the inherited assets for the promotion of new ideas
    \pause
- Diversity of economical and political contexts
  - Monopoly of Germans printers in Venice
  - Bessarion circles between Padoua and Vienna
  - Italian Wars
  - National heritage
*** Methods
***** Collecting data                                     :B_block:BMCOL:
:PROPERTIES:
:BEAMER_env: block
:BEAMER_col: .7
:END:
#+begin_export latex
\begin{center}
\begin{tikzpicture}[mindmap,
grow cyclic,
every node/.style=concept,
concept color=blue!30,
scale=0.5,
transform shape,
level 1/.append style={level distance=5cm,sibling angle=-160},
level 2/.append style={level distance=3cm,sibling angle=-45},
level 3/.append style={level distance=3cm,sibling angle=30},
]
\node{Witness}[clockwise from=10]
child[visible on=<5->] {
    node {Title}[clockwise from=0]
    child[visible on=<8->, concept color=yellow!20] {
        node {\includegraphics[width=1cm]{../images/logos/cerl.jpg}}
    }
    child[visible on=<7->] {
        node {Place}[clockwise from=60]
        child[visible on=<8->, concept color=yellow!20] {
            node {\includegraphics[width=1cm]{../images/logos/geonames.png}}
        }
    }
    child[visible on=<6->] {
        node {Actor}[clockwise from=120]
        child[visible on=<8->, concept color=yellow!20] {
            node {\includegraphics[width=1cm]{../images/logos/oclc.png}}
        }
        child[visible on=<8->, concept color=yellow!20] {
            node {\includegraphics[width=1cm]{../images/logos/idref.png}}
        }
        child[visible on=<8->, concept color=yellow!20] {
            node {\includegraphics[width=1cm]{../images/logos/databnf.png}}
        }
    }
    child[visible on=<5->] {
        node {Corpus}
    }
}
child[visible on=<2->] {
    node {Table}[clockwise from=45]
    child[visible on=<3->] {
        node {TableType}[clockwise from=30]
        child[visible on=<4->] {
            node{AstronomicalObject}[clockwise from=135]
            child[visible on=<8->, concept color=yellow!20] {
                node {\includegraphics[width=1cm]{../images/logos/dishas-dark.png}}
            }
        }
        child[visible on=<3->, sibling angle=-90, concept color=yellow!20] {
            node {\includegraphics[width=1cm]{../images/logos/kanon.png}}
        }
    }
}
child[visible on=<8->, concept color=yellow!20] {
    node {\includegraphics[width=1cm]{../images/logos/iiif.png}}
}
\end{tikzpicture}
\end{center}
#+end_export
***** Analyzing context                                   :B_block:BMCOL:
:PROPERTIES:
:BEAMER_env: block
:BEAMER_col: .3
:BEAMER_ACT: <9->
:END:
- History of printed books
- History of tables
- Actors' Prefaces
*** Sources
***** 6 titles                                                  :B_block:
:PROPERTIES:
:BEAMER_env: block
:END:
#+ATTR_LATEX: :align |c|l|l|c|
|-----------+----------------------+-----------------+--------|
|      Year | Printer              | Canons          | Place  |
|-----------+----------------------+-----------------+--------|
|      1483 | Erhard Ratdolt       | John of Saxony  | Venice |
|           |                      |                 |        |
|-----------+----------------------+-----------------+--------|
|      1492 | Johann Hamman        | J.L. Santritter | Venice |
|           |                      |                 |        |
|-----------+----------------------+-----------------+--------|
| 1518-1521 | Petrus Liechtenstein | J.L. Santritter | Venice |
|           |                      |                 |        |
|-----------+----------------------+-----------------+--------|
|      1524 | Luc'antonio Giunta   | Luca Gaurico    | Venice |
|           |                      |                 |        |
|-----------+----------------------+-----------------+--------|
| 1545-1553 | Chrestien Wechel     | Luca Gaurico    | Paris  |
|           |                      | J.L. Santritter |        |
|-----------+----------------------+-----------------+--------|
| 1640-1641 | /Officina Regia/     | F.G. Venatas    | Madrid |
|-----------+----------------------+-----------------+--------|
* Results
*** Incunabala period (1483-1500)
***** 2 titles                                                  :B_block:
:PROPERTIES:
:BEAMER_env: block
:END:
#+ATTR_LATEX: :align |c|l|l|c|
|-----------+----------------------+-----------------+--------|
|      Year | Printer              | Canons          | Place  |
|-----------+----------------------+-----------------+--------|
|      1483 | Erhard Ratdolt       | John of Saxony  | Venice |
|-----------+----------------------+-----------------+--------|
|      1492 | Johann Hamman        | J.L. Santritter | Venice |
|-----------+----------------------+-----------------+--------|
***** Observations
- Development and protection of innovations for scientific printing
- Campaign for teaching the ideas of Bessarion's protégés in universities
*** Post-incunabala period (1500-1521)
***** 2 titles                                                  :B_block:
:PROPERTIES:
:BEAMER_env: block
:END:
#+ATTR_LATEX: :align |c|l|l|c|
|-----------+----------------------+-----------------+--------|
|      Year | Printer              | Canons          | Place  |
|-----------+----------------------+-----------------+--------|
| 1518-1521 | Petrus Liechtenstein | J.L. Santritter | Venice |
|-----------+----------------------+-----------------+--------|
|      1524 | Luc'antonio Giunta   | Luca Gaurico    | Venice |
|-----------+----------------------+-----------------+--------|
***** Observations
- New stakes in court astronomy during Italian Wars
- Growing influence of astrology
- Evolution of typographic standards
*** Last editions (1545-1641)
***** 2 titles                                                  :B_block:
:PROPERTIES:
:BEAMER_env: block
:END:
#+ATTR_LATEX: :align |c|l|l|c|
|-----------+----------------------+-----------------+--------|
|      Year | Printer              | Canons          | Place  |
|-----------+----------------------+-----------------+--------|
| 1545-1553 | Chrestien Wechel     | Luca Gaurico    | Paris  |
|           |                      | J.L. Santritter |        |
|-----------+----------------------+-----------------+--------|
| 1640-1641 | /Officina Regia/     | F.G. Venatas    | Madrid |
|-----------+----------------------+-----------------+--------|
***** Observations
- Two isolated editions, far from Italy
  - Paris: probable influence of Oronce Finés' teaching of Peurbach and Regiomontanus at the /Collège Royal/
  - Madrid: promotion of a Castilian heritage
* Conclusion
*** Some key points
- Associating the name of Alfonse to the tables is not a neutral choice
- Differences between print and manuscript cultures
  - Printers are closely connected to humanists
  - Printed editions don't seem to be part of a "toolbox"
  - Printers never intend to use the tables
- Astrology is an important concern during the 16th century
- Venice was an important place from where printed editions were broadcasted
  - Other places are lesser known (Paris, Madrid, but also Hungria, Constantinople/Istambul,
    Germany, Poland, North Africa, ...)
- A Christian-centric historiography that obscures Orthodox, Jews, Muslims, and other astronomical traditions
*** Personnal lessons
***** Begin_list                                        :B_ignoreheading:
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:
- Focus on a smaller time period, more adapted to the historical microscope
  #+beamer: \pause
***** Sanity equations                                     :B_definition:
:PROPERTIES:
:BEAMER_env: definition
:BEAMER_ACT: <only@2>
:END:
  \[\text{work time needed}=\text{period timespan}\times\text{historical magnification}\]
  \[\text{period timespan}=\frac{\text{work time available}}{\text{historical magnification}}\]
  \[\text{historical magnification}=\frac{\text{work time available}}{\text{period timespan}}\]
***** Continue list                                     :B_ignoreheading:
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:
#+beamer: \pause
- Manage time
  #+ATTR_BEAMER: :overlay <only@3>
  - Set aside time for mathematical computations
  - Keep time for writing and proofreading
  - Get sooner in touch with researchers
  - Have flexible objectives and deadlines
  #+beamer: \pause
- Keep a better track of the works and ideas
  #+ATTR_BEAMER: :overlay <only@4>
  - Divide the project in smaller tasks
  - Take notes while navigating between primary and secondary sources
  - Keep translations and transcriptions organized
  #+beamer: \pause
- Exploit available resources
  #+ATTR_BEAMER: :overlay <only@5>
  - Understand actors biographies, networks, environments
  - Use archeological sources and explore material aspects of the sources
  - Have reviewers
  #+beamer: \pause
- Start (re)writing earlier

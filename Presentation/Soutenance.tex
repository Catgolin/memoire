% Created 2023-05-31 mer. 14:12
% Intended LaTeX compiler: pdflatex
\documentclass[presentation]{beamer}
\usepackage[LAE, LGR, T2A, T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage[french]{babel}
\usepackage{setspace}
\usepackage{imakeidx}
\usepackage{textalpha}
\usepackage{arabtex}
\usepackage{utf8}
\usepackage[newfloat]{minted}
\usepackage[hidelinks]{hyperref}
\institute{M2 Histoire et philosophie des sciences\\ Université Paris Cité\\ Sous la direction de\\Matthieu Husson (Syrte, Observatoire de Paris, CNRS)\\et\\Sara Confalonieri (Sphere, Université Paris Cité, CNRS)}
\mode<beamer>{\usetheme{Antibes}}
\mode<beamer>{\usecolortheme{beaver}}
\AtBeginSection[]{\begin{frame}<beamer>\frametitle{}\tableofcontents[currentsection]\end{frame}}
\setbeamerfont{caption}{size=\tiny}
\setbeamertemplate{footline}[frame number]
\usepackage{tikz}
\usetikzlibrary{mindmap}
\usepackage{txfonts}
\usetheme{default}
\author{Clément Cartier <contact@catgolin.eu>}
\date{\textit{<2023-05-31 mer. 14:30>}}
\title{Astronomie médiévale des presses humanistes à l'âge classique}
\subtitle{Patrimonialisation des tables alphonsines dans la culture des imprimés (1483--1641)}
\hypersetup{
 pdfauthor={Clément Cartier <contact@catgolin.eu>},
 pdftitle={Astronomie médiévale des presses humanistes à l'âge classique},
 pdfkeywords={Histoire de l'astronomie, Histoire des mathématiques, Moyen Âge, Histoire du livre,},
 pdfsubject={Soutenance},
 pdfcreator={Emacs 27.1 (Org mode 9.6-pre)}, 
 pdflang={French}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\maketitle
% source: stackoverflow
\tikzset{
  invisible/.style={opacity=0, text opacity=0},
  visible on/.style={alt={#1{}{invisible}}},
  alt/.code args={<#1>#2#3}{%
    \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}} %
  },
}

\section{Introduction}
\label{sec:org486744c}
\begin{frame}[label={sec:org67fddb7}]{Problem statement and thesis}
\begin{block}{Problem statement}
\begin{itemize}
\item How did the \og Alfonsine Tables\fg{} evolve after they started to be printed
\end{itemize}
\pause
\end{block}
\begin{block}{Thesis}
\begin{itemize}
\item \og Humanist\fg{} goals infused in the \og print cultures\fg{}
\begin{itemize}
\item Transmission of a heritage from the past
\item Exploiting the inherited assets for the promotion of new ideas
\pause
\end{itemize}
\item Diversity of economical and political contexts
\begin{itemize}
\item Monopoly of Germans printers in Venice
\item Bessarion circles between Padoua and Vienna
\item Italian Wars
\item National heritage
\end{itemize}
\end{itemize}
\end{block}
\end{frame}
\begin{frame}[label={sec:org9d447d8}]{Methods}
\begin{columns}
\begin{column}{.7\columnwidth}
\begin{block}{Collecting data}
\begin{center}
\begin{tikzpicture}[mindmap,
grow cyclic,
every node/.style=concept,
concept color=blue!30,
scale=0.5,
transform shape,
level 1/.append style={level distance=5cm,sibling angle=-160},
level 2/.append style={level distance=3cm,sibling angle=-45},
level 3/.append style={level distance=3cm,sibling angle=30},
]
\node{Witness}[clockwise from=10]
child[visible on=<5->] {
    node {Title}[clockwise from=0]
    child[visible on=<8->, concept color=yellow!20] {
        node {\includegraphics[width=1cm]{../images/logos/cerl.jpg}}
    }
    child[visible on=<7->] {
        node {Place}[clockwise from=60]
        child[visible on=<8->, concept color=yellow!20] {
            node {\includegraphics[width=1cm]{../images/logos/geonames.png}}
        }
    }
    child[visible on=<6->] {
        node {Actor}[clockwise from=120]
        child[visible on=<8->, concept color=yellow!20] {
            node {\includegraphics[width=1cm]{../images/logos/oclc.png}}
        }
        child[visible on=<8->, concept color=yellow!20] {
            node {\includegraphics[width=1cm]{../images/logos/idref.png}}
        }
        child[visible on=<8->, concept color=yellow!20] {
            node {\includegraphics[width=1cm]{../images/logos/databnf.png}}
        }
    }
    child[visible on=<5->] {
        node {Corpus}
    }
}
child[visible on=<2->] {
    node {Table}[clockwise from=45]
    child[visible on=<3->] {
        node {TableType}[clockwise from=30]
        child[visible on=<4->] {
            node{AstronomicalObject}[clockwise from=135]
            child[visible on=<8->, concept color=yellow!20] {
                node {\includegraphics[width=1cm]{../images/logos/dishas-dark.png}}
            }
        }
        child[visible on=<3->, sibling angle=-90, concept color=yellow!20] {
            node {\includegraphics[width=1cm]{../images/logos/kanon.png}}
        }
    }
}
child[visible on=<8->, concept color=yellow!20] {
    node {\includegraphics[width=1cm]{../images/logos/iiif.png}}
}
\end{tikzpicture}
\end{center}
\end{block}
\end{column}
\begin{column}{.3\columnwidth}
\begin{block}<9->{Analyzing context}
\begin{itemize}
\item History of printed books
\item History of tables
\item Actors' Prefaces
\end{itemize}
\end{block}
\end{column}
\end{columns}
\end{frame}
\begin{frame}[label={sec:org9d72242}]{Sources}
\begin{block}{6 titles}
\begin{center}
\begin{tabular}{|c|l|l|c|}
\hline
Year & Printer & Canons & Place\\
\hline
1483 & Erhard Ratdolt & John of Saxony & Venice\\
 &  &  & \\
\hline
1492 & Johann Hamman & J.L. Santritter & Venice\\
 &  &  & \\
\hline
1518-1521 & Petrus Liechtenstein & J.L. Santritter & Venice\\
 &  &  & \\
\hline
1524 & Luc'antonio Giunta & Luca Gaurico & Venice\\
 &  &  & \\
\hline
1545-1553 & Chrestien Wechel & Luca Gaurico & Paris\\
 &  & J.L. Santritter & \\
\hline
1640-1641 & \emph{Officina Regia} & F.G. Venatas & Madrid\\
\hline
\end{tabular}
\end{center}
\end{block}
\end{frame}
\section{Results}
\label{sec:org2da1b84}
\begin{frame}[label={sec:org825a899}]{Incunabala period (1483-1500)}
\begin{block}{2 titles}
\begin{center}
\begin{tabular}{|c|l|l|c|}
\hline
Year & Printer & Canons & Place\\
\hline
1483 & Erhard Ratdolt & John of Saxony & Venice\\
\hline
1492 & Johann Hamman & J.L. Santritter & Venice\\
\hline
\end{tabular}
\end{center}
\end{block}
\begin{block}{Observations}
\begin{itemize}
\item Development and protection of innovations for scientific printing
\item Campaign for teaching the ideas of Bessarion's protégés in universities
\end{itemize}
\end{block}
\end{frame}
\begin{frame}[label={sec:org264149e}]{Post-incunabala period (1500-1521)}
\begin{block}{2 titles}
\begin{center}
\begin{tabular}{|c|l|l|c|}
\hline
Year & Printer & Canons & Place\\
\hline
1518-1521 & Petrus Liechtenstein & J.L. Santritter & Venice\\
\hline
1524 & Luc'antonio Giunta & Luca Gaurico & Venice\\
\hline
\end{tabular}
\end{center}
\end{block}
\begin{block}{Observations}
\begin{itemize}
\item New stakes in court astronomy during Italian Wars
\item Growing influence of astrology
\item Evolution of typographic standards
\end{itemize}
\end{block}
\end{frame}
\begin{frame}[label={sec:org455c059}]{Last editions (1545-1641)}
\begin{block}{2 titles}
\begin{center}
\begin{tabular}{|c|l|l|c|}
\hline
Year & Printer & Canons & Place\\
\hline
1545-1553 & Chrestien Wechel & Luca Gaurico & Paris\\
 &  & J.L. Santritter & \\
\hline
1640-1641 & \emph{Officina Regia} & F.G. Venatas & Madrid\\
\hline
\end{tabular}
\end{center}
\end{block}
\begin{block}{Observations}
\begin{itemize}
\item Two isolated editions, far from Italy
\begin{itemize}
\item Paris: probable influence of Oronce Finés' teaching of Peurbach and Regiomontanus at the \emph{Collège Royal}
\item Madrid: promotion of a Castilian heritage
\end{itemize}
\end{itemize}
\end{block}
\end{frame}
\section{Conclusion}
\label{sec:org7e5d4f8}
\begin{frame}[label={sec:orgdc2636f}]{Some key points}
\begin{itemize}
\item Associating the name of Alfonse to the tables is not a neutral choice
\item Differences between print and manuscript cultures
\begin{itemize}
\item Printers are closely connected to humanists
\item Printed editions don't seem to be part of a \og toolbox\fg{}
\item Printers never intend to use the tables
\end{itemize}
\item Astrology is an important concern during the 16th century
\item Venice was an important place from where printed editions were broadcasted
\begin{itemize}
\item Other places are lesser known (Paris, Madrid, but also Hungria, Constantinople/Istambul,
Germany, Poland, North Africa, \ldots{})
\end{itemize}
\item A Christian-centric historiography that obscures Orthodox, Jews, Muslims, and other astronomical traditions
\end{itemize}
\end{frame}
\begin{frame}[label={sec:org7362ddf}]{Personnal lessons}
\begin{itemize}
\item Focus on a smaller time period, more adapted to the historical microscope
\pause
\end{itemize}
\begin{definition}<only@2>[Sanity equations]
\[\text{work time needed}=\text{period timespan}\times\text{historical magnification}\]
\[\text{period timespan}=\frac{\text{work time available}}{\text{historical magnification}}\]
\[\text{historical magnification}=\frac{\text{work time available}}{\text{period timespan}}\]
\end{definition}
\pause
\begin{itemize}
\item Manage time
\begin{itemize}[<only@3>]
\item Set aside time for mathematical computations
\item Keep time for writing and proofreading
\item Get sooner in touch with researchers
\item Have flexible objectives and deadlines
\end{itemize}
\pause
\item Keep a better track of the works and ideas
\begin{itemize}[<only@4>]
\item Divide the project in smaller tasks
\item Take notes while navigating between primary and secondary sources
\item Keep translations and transcriptions organized
\end{itemize}
\pause
\item Exploit available resources
\begin{itemize}[<only@5>]
\item Understand actors biographies, networks, environments
\item Use archeological sources and explore material aspects of the sources
\item Have reviewers
\end{itemize}
\pause
\item Start (re)writing earlier
\end{itemize}
\end{frame}
\end{document}
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib.offsetbox import OffsetImage, AnnotationBbox

saw_logo = mpimg.imread("logo-saw.png")
saw_img = OffsetImage(saw_logo, zoom = 0.2)

alfa_logo = mpimg.imread("logo-alfa.png")
alfa_img = OffsetImage(alfa_logo, zoom = 0.2)


dates = [1984, 1988, 1998, 2009, 2012, 2016, 2022]

fig, ax = plt.subplots()
plt.axis('off')

legend_dist = 0.01
legend_length = legend_dist - 0.003
lw = 6

plt.annotate(
	"Emmanuel Poulle",
	xy=(1984, 0),
	xytext=(1984, legend_length),
	horizontalalignment="center",
	verticalalignment = "top",
	arrowprops=dict(arrowstyle="->"),
)
plt.annotate(
	"Emmanuel Poulle",
	xy=(1998, 0),
	xytext=(1984, legend_length),
	horizontalalignment="center",
	verticalalignment = "top",
	arrowprops=dict(arrowstyle="->", linestyle="dashed"),
)
plt.annotate(
	"Emmanuel Poulle\net Denis Savoie",
	xy=(1998, 0),
	xytext=(1998, legend_length),
	horizontalalignment="center",
	verticalalignment = "top",
	arrowprops=dict(arrowstyle="->"),
)
plt.annotate(
	"José Chabás\net Bernard Goldstein",
	xy=(1988, 0),
	xytext=(1988, -legend_length),
	horizontalalignment="center",
	verticalalignment = "top",
	arrowprops=dict(arrowstyle="->"),
)
plt.annotate(
	"En cours",
	xy=(1988, -0.002),
	xytext=(2000, -0.002),
	arrowprops=dict(arrowstyle="<-", linestyle="dotted"),
)

plt.plot([2009, 2012], [0, 0], color="y", linewidth=lw)
plt.plot([2010.5, 2010.5], [0, -legend_length], color="y")
plt.annotate(
	"Histoire des\nTables\nNumériques",
	xy=(2010.5, 0),
	xytext=(2010.5, -legend_length),
	horizontalalignment="center",
	verticalalignment = "top",
)

plt.plot([2012, 2016], [0, 0], color="r", linewidth=lw)
plt.plot([2014, 2014], [0, legend_dist], color="r")
saw_box = AnnotationBbox(saw_img, (2014, legend_dist), frameon=False)
ax.add_artist(saw_box)

plt.plot([2016, 2022], [-0, -0], color="b", linewidth=lw)
plt.plot([2020, 2020], [-0, -legend_dist], color="b")
alfa_box = AnnotationBbox(alfa_img, (2020, -legend_dist), frameon=False)
ax.add_artist(alfa_box)

plt.plot([1980, 2023], [0, 0], color="k")
plt.plot(dates,
		 [0 for d in dates],
		 "-o",
		 color="k",
		 markerfacecolor="w")

i = 0
for d in dates:
	plt.annotate(
		str(d),
		xy=(d, 0),
		xytext=(0, -3 * ((-1) ** i)),
		textcoords="offset points",
		horizontalalignment="center",
		verticalalignment="top" if 0 == i % 2 else "bottom",
	)
	i += 1

plt.show()

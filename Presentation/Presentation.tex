% Created 2023-02-21 mar. 19:47
% Intended LaTeX compiler: pdflatex
\documentclass[12pt]{article}
\usepackage[LAE, LGR, T2A, T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage[hidelinks]{hyperref}
\usepackage[french]{babel}
\usepackage{setspace}
\usepackage{textalpha}
\usepackage{arabtex}
\usepackage{utf8}
\usepackage{enotez}
\let\footnote=\endnote
\author{Clément Cartier\thanks{contact@catgolin.eu}}
\date{2023-02-24 ven.}
\title{Astronomie médiévale sous les presses humanistes\\\medskip
\large Patrimonialisation des tables alphonsines dans la culture des imprimés (1483-1641)}
\hypersetup{
 pdfauthor={Clément Cartier},
 pdftitle={Astronomie médiévale sous les presses humanistes},
 pdfkeywords={Histoire de l'astronomie, Mathématiques pratiques au Moyen-Âge, Histoire du livre, Renaissance, Humanités numériques,},
 pdfsubject={Présentation de mémoire à mi-parcours},
 pdfcreator={Emacs 27.1 (Org mode 9.6-pre)}, 
 pdflang={French}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\begin{titlepage}
\centering
\vspace{0.5cm}
{\huge\scshape\bfseries Astronomie médiévale sous les presses humanistes.\par}
\vspace{0.25cm}
{\Large\bfseries Patrimonialisation des tables alphonsines dans la culture des imprimés (1483-1641).\par}
\vspace{1cm}
{\large Présentation de mémoire à mi-parcours\par}
\vspace{0.5cm}
{(2023-02-24 ven.)\par}
\vfill
{\large Clément Cartier\par}
{\itshape<\href{mailto:contact@catgolin.eu}{contact@catgolin.eu}>\par}
\vspace{0.25cm}
{\itshape M2 Histoire et philosophie des sciences\par Université Paris Cité\par}
\vfill
Sous la direction de\par
Matthieu Husson {\itshape(Syrte, Observatoire de Paris, CNRS)}\par
Sara Confalonieri {\itshape(Sphere, Université Paris Cité, CNRS)}\par
\vfill
{\small Mots clefs : Histoire de l'astronomie, Mathématiques pratiques au Moyen-Âge, Histoire du livre, Renaissance, Humanités numériques,\par}
\end{titlepage}

\section{Introduction}
\label{sec:org8f392e7}
Dans le titre provisoire de mon mémoire,
«Astronomie médiévale sous les presses humanistes»,
la partie de l'astronomie médiévale qui m'intéresse particulièrement
sont les tables alphonsines:
elles sont le principal outil pour les calculs liés aux planètes
et leurs applications à partir du \textsc{xiv}e siècle en Europe.
Mon but est de produire un travail qui s'inscrit dans le cadre
de l'histoire de l'astronomie sur les versions imprimées
de ces tables autour du \textsc{xvi}e siècle,
principalement dans les courants dits «humanistes»,
qui se situent en Europe et qui sont attachés à l'idée
de transmettre une culture principalement latine.

Pour ce faire, je m'appuie notamment sur des travaux liés à
l'histoire du livre qui analysent ce qu'ils appellent
la «culture des imprimés»,
pour comprendre le milieu de production de ces éditions des tables.

Je cherche à étudier la manière
dont les acteurs et actrices reçoivent, adaptent et transmettent
une tradition littéraire et scientifique initialement dédiée
à des pratiques de calcul astronomique,
dans un contexte où d'autres modèles pour l'astronomie émergent.
C'est ce processus de réception, réappropriation, adaptation
et de transmission des tables que je désigne sous le terme
de «patrimonialisation», et j'essaye montrer
en quoi l'imprimerie a accompagné ce processus
dans un contexte où les tables ci s'éloignent peu à peu
de leurs usages premiers.

\section{Contexte}
\label{sec:orgd853870}
À l'origine, les tables alphonsines sont produites
vers 1260 à la cour d'Alphonse X de Castille,
en compilant des sources qui circulent
dans la péninsule Ibérique et des observations du ciel.
Mais c'est surtout le modèle parisien,
composé en latin autour de l'université de Paris vers 1321,
avec ses différentes variations,
qui s'impose, dès le \textsc{xiv}e siècle,
comme l'instrument incontournable de tout calcul astronomique.

En général, les tables sont toujours accompagnées
de canons, des textes qui expliquent les procédures
de calcul et de lecture des tables.
Or, les calculs impliquent en général des procédures
imbriquées les unes dans les autres, et mobilisent
plusieurs tables.
Ainsi, une table n'est jamais seule:
les tables alphonsines constituent un ensemble
de tables et de canons relié comme un tout
plus-ou-moins cohérent par de nombreuses références internes,
mais dont les frontières sont floues et variables.

La première version imprimée des tables alphonsines
date de 1483, et on en trouve plusieurs rééditions
jusqu'en 1641:
ce sont ces versions imprimées qui m'intéressent.
\section{État de l'art}
\label{sec:orgc0a00b6}
\subsection{Étude des tables astronomique}
\label{sec:org19e55e0}
Les premiers travaux historiques sérieux qui s'intéressent
en détail au contenu des tables alphonsines
sont dûs à Emmanuel Poulle, dans les années 1980-90.
[\citeprocitem{7}{7}–\citeprocitem{9}{9}].

Par la suite, au sein de Sphere,
plusieurs projets se sont intéressés aux tables:
d'abord le projet Histoire des tables numériques,
qui s'est poursuivi dans l'axe 2 du projet
Sciences mathématiques dans les mondes anciens.
Le projet ALFA porté par Matthieu Husson entre 2016 et 2022
a pris la suite de ces travaux pour se concentrer entièrement
aux tables alphonsines.
Il développe notamment tout un ensemble d'outils
liés aux humanités numériques
que je peux utiliser pour mon mémoire.

Mais jusqu'à maintenant,
les études se sont surtout focalisés
sur les sources manuscrites et moins sur les imprimés.
\subsection{Histoire des imprimés}
\label{sec:org97673bd}
Depuis quelques années,
une nouvelle historiographie de l'imprimerie
inscrite dans la continuité des travaux
d'Elizabeth Eisenstein [\citeprocitem{2}{2}, \citeprocitem{3}{3}],
dessine les contours de la \emph{print culture},
une «culture des imprimés» caractérisée par des transformations
des mentalités liées au statut d'ouvrage, d'auteur,
et surtout liées à la transmission
et à la maintenance de ces statuts qui sont pour moi
au cœur du processus de patrimonialisation.

Dans le cadre de ce courant,
Isabelle Pantin a commencé à s'intéresser
aux diagrammes dans les livres d'astronomie
[\citeprocitem{5}{5}, \citeprocitem{4}{4}].
Elle propose notamment une périodisation
de l'évolution des pratiques, des formats et l'économie
de l'impression des ouvrages d'astronomie
que je pense pouvoir réutiliser pour l'évolution
des impressions des tables alphonsines.
\section{Problématique}
\label{sec:orgb8e0e21}
\subsection{Problématique}
\label{sec:org4339095}
Pour étudier les éditions imprimées des tables alphonsines
dans le cadre de la culture des imprimés,
je me pose notamment la question des valeurs pratiques
et patrimoniales qui leur sont attachées.

Mon hypothèse est que les tables alphonsines
sont à l'origine des instruments de calcul
que les acteurs et actrices à l'époque des premières éditions
cherchent à patrimonialiser
sans les séparer de leurs usages pratiques.
Mais, à mesure que la valeur de leurs usages
en tant qu'instruments de calcul est réduite
par la promotion d'autes modèles,
leur valeur patrimoniale devient peu à peu
la principale motivation des éditions.

Je cherche donc à montrer en quoi la culture des imprimés
a-t-elle contribué à patrimonialiser un instrument de calcul
du Moyen-Âge.
\subsection{Intérêt}
\label{sec:org09d0724}
C'est un travail qui s'inscrit dans le cadre d'un projet
plus large qui cherche à décrire l'évolution
de la tradition alphonsine,
en faisant la transition entre
d'une part l'étude des tables dans les manuscrits,
et d'autre part l'étude de l'historiographie des tables,
depuis les premiers projets d'histoire de l'astronomie
au \textsc{xv}e siècle jusqu'à Emmanuel Poulle.

\section{Sources}
\label{sec:orgdf48b81}
Pour mon travail,
j'étudie un corpus qui comporte une quarantaine d'imprimés,
principalement en latin et presque intégralement numérisé,
identifés dans la base de données du projet ALFA.

Ces sources sont produites entre 1483 jusqu'en 1641,
mais plus de 80\% sont produites avant le \emph{De revolutionibus}
en 1543.

Les éditeurs de ces imprimés se trouvent majoritairement
à Venise (35\%), puis dans le Saint-Empire Romain Germanique (40\%);
dans une moindre mesure en France (entre 7\% et 10\%\footnote{Selon
le point de vue que l'on adopte sur l'allégence du Duc de Bourgogne
entre 1520 et 1530}), en Suisse, au Portugal et en Espagne.

J'ai déjà pu identifier
plusieurs sous-ensembles au sein de ce corpus,
qui présentent plus ou moins directement leur appartenance
au corpus alphonsin,
ce qui me permet de restreindre le corpus à un ensemble
plus concis et de l'étendre progressivement en fonction du temps
dont je pourrai disposer.
\section{Plan}
\label{sec:orgf9fb979}

\subsection{Plan de travail}
\label{sec:org9f9fe97}
Mon travail consiste dans un premier temps
à identifier le contenu des tables présentées dans les imprimés,
en utilisant les classifications proposées par
José Chabás [\citeprocitem{1}{1}] et
Emmanuel Poulle [\citeprocitem{6}{6}].

Une fois les tables identifiées,
mon but sera de repérer les différentes empreintes
identifées par Richard Kremer dans les tables manuscrites
pour comprendre le lien qu'entretiennent les tables imprimées
avec les manuscrits.

Enfin, en parallèle de ce travail,
je cherche à analyser le discours des acteurs et actrices,
notamment dans les préfaces des différentes éditions.
\subsection{Plan prévisionnel}
\label{sec:org399e8d7}
À ce stade de ma réflexion, je pense pouvoir organiser
mon mémoire autour de quatre périodes qui semblent
se dégager de la première analyse quantitative que j'ai pu mener
sur les sources,
et qui correspondent reltivement bien aux périodes
identifiées par Isabelle Pantin pour les imprimés géométriques.

Dans les années 1480-1490, les premières éditions imprimées
sont produites, notamment à Venise autour d'Erhard Ratdolt,
et d'autres acteurs et actrices qui mettent en place
les outils nécessaires à l'impression de ce genre d'objets.

Dans les années suivantes, on semble assister
à une forte montée en puissance
de la tradition alphonsine imprimée jusqu'en 1520,
où de nouvelles approches de l'astronomie se développent.

On assiste alors au contraire à une diminution progressive
du nombre d'éditions de tables alphonsines,
avec au contraire le développement des éditions des tables
dérivées, probablement sous l'influence de Johannes Schöner.

Enfin, après 1543, plus aucune édition des tables alphonsines
ne semble être destinées à un usage pratique,
et la tradition alphonsine disparaît complètement
après l'édition de 1641.
\section{Bibliographie}
\label{sec:orga348eaa}
\hypertarget{citeproc_bib_item_1}{[1] José Chabás, \textit{Computational Astronomy in the Middle Ages, Sets of Astronomical Tables in Latin} Madrid, Espagne : Consejo Superior de Investigaciones Científicas, 2019.}

\hypertarget{citeproc_bib_item_2}{[2] Elizabeth Eisenstein, Anthony Grafton, \& Adrian Johns, “How Revolutionary Was the Print Revolution?” in. \textit{American historical review},. vol. 107,. pp. 84–128. 2002.}

\hypertarget{citeproc_bib_item_3}{[3] Anthony Grafton, “The Importance of Being Printed.” in. \textit{Journal of interdisciplinary history},. vol. 11,. pp. 265–286. 1980.}

\hypertarget{citeproc_bib_item_4}{[4] Isabelle Pantin, “Borrowers and Innovators in the History of Printing Sacrobosco: The Case of the in-Octavo Tradition.” in. Matteo Valleriani (ed.) . \textit{De Sphaera of Johannes de Sacrobosco in the Early Modern Period: The Authors of the Commentaries}. pp. 265–312. Cham, Zoug (Confédération Suisse) : Springer Cham,. 2020.}

\hypertarget{citeproc_bib_item_5}{[5] Isabelle Pantin, “The First Phases of the Theoricae Planetarum Printed Tradition (1474-1535): The Evolution of a Genre Observed through Its Images.” in. \textit{Journal for the history of astronomy},. vol. 43,. pp. 3–26. 2012.}

\hypertarget{citeproc_bib_item_6}{[6] Emmanuel Poulle (ed.), \textit{Les tables alphonsines avec les canons de Jean de Saxe} Paris, France : Éditions du Centre national de la recherche scientifique, 1984.}

\hypertarget{citeproc_bib_item_7}{[7] Emmanuel Poulle, \textit{Les instruments de la théorie des planètes selon Ptolémée: équatoires et horlogeries planétaires du xiiie au xvie siècle} Genève, Suisse : Droz, 1980.}

\hypertarget{citeproc_bib_item_8}{[8] Emmanuel Poulle \& Owen Gingerich, \href{https://doi.org/10.3406/crai.1967.12173}{“Les positions des planètes au Moyen-Âge: applicatino du calcul électronique aux tables alphonsines.}” in. \textit{Comptes rendus des séances de l’académie des inscriptions et belles-lettres},. vol. 111,. no. 4. pp. 531–548. 1967.}

\hypertarget{citeproc_bib_item_9}{[9] Emmanuel Poulle \& Denis Savoie, “La survie de l’astronomie alphonsine.” in. \textit{Journal for the history of astronomy},. vol. 29,. pp. 201–207. 1998.}



\printendnotes
\end{document}